import React from "react"
import { createStackNavigator } from "@react-navigation/stack"
import { DemoScreen } from "../screens"

export type PrimaryParamList = {
  demo: undefined
}

const Stack = createStackNavigator()

export function ProfileNavigator() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="demo" component={DemoScreen} />
    </Stack.Navigator>
  )
}