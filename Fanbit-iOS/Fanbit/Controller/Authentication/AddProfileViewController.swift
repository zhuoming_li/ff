//
//  AddProfileViewController.swift
//  Fanbit
//
//  Created by fanbit on 2021/1/9.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseStorage
import RSKImageCropper

class AddProfileViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate, RSKImageCropViewControllerDelegate {
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    lazy var imagePicker = UIImagePickerController()
    var chosenImage: UIImage?
    var bPhotoSelected = false
    var email:String?
    var password:String?
    var username:String?
    var usertype:String?
    var credential:AuthCredential?
    var isFrom:String?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func clickAddPhoto(_ sender: UIButton) {
        imagePicker.delegate = self
        let alert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
            let takePhoto = UIAlertAction.init(
                title: "Take Photo",
                style: .default,
                handler: { (UIAlertAction) -> Void in
                    self.imagePicker.sourceType = .camera
                    self.present(self.imagePicker, animated: true, completion: nil)
            })
            let choosePhotoAction = UIAlertAction.init(
                title: "Choose From Library",
                style: .default,
                handler: { (UIAlertAction) -> Void in
                    self.imagePicker.sourceType = .photoLibrary
                    self.present(self.imagePicker, animated: true, completion: nil)
            })
            let cancel = UIAlertAction.init(
                title: "Cancel",
                style: .cancel,
                handler: nil)
            alert.addAction(takePhoto)
            alert.addAction(choosePhotoAction)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func nextClicked(_ sender: Any) {
        if !bPhotoSelected {
            imagePicker.delegate = self
            let alert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
                let takePhoto = UIAlertAction.init(
                    title: "Take Photo",
                    style: .default,
                    handler: { (UIAlertAction) -> Void in
                        self.imagePicker.sourceType = .camera
                        self.present(self.imagePicker, animated: true, completion: nil)
                })
                let choosePhotoAction = UIAlertAction.init(
                    title: "Choose From Library",
                    style: .default,
                    handler: { (UIAlertAction) -> Void in
                        self.imagePicker.sourceType = .photoLibrary
                        self.present(self.imagePicker, animated: true, completion: nil)
                })
                let cancel = UIAlertAction.init(
                    title: "Cancel",
                    style: .cancel,
                    handler: nil)
                alert.addAction(takePhoto)
                alert.addAction(choosePhotoAction)
                alert.addAction(cancel)
                self.present(alert, animated: true, completion: nil)
            return
        }
        
        showHUD()
        if self.isFrom == "Social"{
            FireAuth.shared.loginWith(credential: self.credential!, completion: { (result, error) in
                if let _ = result {
                    
                        // set profile image
                        guard let profileImg = self.btnProfile.currentImage else { return }
                        guard let uploadData = profileImg.jpegData(compressionQuality: 0.3) else { return }

                        let filename = NSUUID().uuidString
                        let storageRef = STORAGE_PROFILE_IMAGES.child(filename)
                        self.showHUD()
                        storageRef.putData(uploadData, metadata: nil, completion: { (metadata, error) in
                            self.hideHUD()
                            // handle error
                            if let error = error {

                                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                                let ok = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
                                alert.addAction(ok)
                                self.present(alert, animated: true, completion: nil)
                                print("Failed to upload image to Firebase Storage with error", error.localizedDescription)
                                return
                            }

                            // UPDATE: - Firebase 5 must now retrieve download url
                            self.showHUD()
                            storageRef.downloadURL(completion: { (downloadURL, error) in
                                self.hideHUD()
                                if let error = error {

                                    let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                                    let ok = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
                                    alert.addAction(ok)
                                    self.present(alert, animated: true, completion: nil)
                                    print(error.localizedDescription)
                                    return
                                }

                                guard let profileImageUrl = downloadURL?.absoluteString else {
                                    print("DEBUG: Profile image url is nil")
                                    return
                                }
                                // user id
                                guard let uid = result?.user.uid else { return }
                                let dictionaryValues = ["usertype" : self.usertype ?? "", "bio":"",
                                                        "profileImageUrl" : profileImageUrl,
                                                        "createdAt": Int(NSDate().timeIntervalSince1970)] as [String : Any]

                                let values = [uid: dictionaryValues]
                                // save user info to database
                                REF_USERS.updateChildValues(values, withCompletionBlock: { (error, ref) in
                                    if let error = error {

                                        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                                        let ok = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
                                        alert.addAction(ok)
                                        self.present(alert, animated: true, completion: nil)
                                        print(error.localizedDescription)
                                        return
                                    }
                                    self.navigateToHome()
                                })
                            })
                        })

                } else {
                    self.showErrorAlert(message: error?.localizedDescription)
                }
                self.hideHUD()
            })

        } else {
            guard let email = self.email else { return }
            FireAuth.shared.registerUserWith(email: email, password: self.password!) { (result, error) in
                self.hideHUD()
                if let _ = result {
                    // set profile image
                    guard let profileImg = self.btnProfile.currentImage else { return }
                    guard let uploadData = profileImg.jpegData(compressionQuality: 0.3) else { return }

                    let filename = NSUUID().uuidString
                    let storageRef = STORAGE_PROFILE_IMAGES.child(filename)
                    self.showHUD()
                    storageRef.putData(uploadData, metadata: nil, completion: { (metadata, error) in
                        self.hideHUD()
                        // handle error
                        if let error = error {

                            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                            let ok = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
                            alert.addAction(ok)
                            self.present(alert, animated: true, completion: nil)
                            print("Failed to upload image to Firebase Storage with error", error.localizedDescription)
                            return
                        }

                        // UPDATE: - Firebase 5 must now retrieve download url
                        self.showHUD()
                        storageRef.downloadURL(completion: { (downloadURL, error) in
                            self.hideHUD()
                            if let error = error {

                                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                                let ok = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
                                alert.addAction(ok)
                                self.present(alert, animated: true, completion: nil)
                                print(error.localizedDescription)
                                return
                            }

                            guard let profileImageUrl = downloadURL?.absoluteString else {
                                print("DEBUG: Profile image url is nil")
                                return
                            }
                            // user id
                            guard let uid = result?.user.uid else { return }
                            let dictionaryValues = ["username" : self.username ?? "","usertype" : self.usertype ?? "","email" : self.email ?? "", "bio":"",
                                                    "profileImageUrl" : profileImageUrl,
                                                    "createdAt": Int(NSDate().timeIntervalSince1970)] as [String : Any]

                            let values = [uid: dictionaryValues]
                            // save user info to database
                            REF_USERS.updateChildValues(values, withCompletionBlock: { (error, ref) in
                                if let error = error {

                                    let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                                    let ok = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
                                    alert.addAction(ok)
                                    self.present(alert, animated: true, completion: nil)
                                    print(error.localizedDescription)
                                    return
                                }
                                self.navigateToHome()
                            })
                        })
                    })
                }
                self.showAlertErrorIfNeeded(error: error)
            }
        }
    }
    fileprivate func navigateToHome() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "MainTabBarController") as! MainTabBarController
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    @IBAction func skipClicked(_ sender: Any) {
        guard let email = self.email else { return }
        FireAuth.shared.registerUserWith(email: email, password: self.password!) { (result, error) in
            self.hideHUD()
            if let _ = result {
                // set profile image
                guard let profileImg = UIImage.init(named: "camera") else { return }
                guard let uploadData = profileImg.jpegData(compressionQuality: 0.3) else { return }

                let filename = NSUUID().uuidString
                let storageRef = STORAGE_PROFILE_IMAGES.child(filename)
                self.showHUD()
                storageRef.putData(uploadData, metadata: nil, completion: { (metadata, error) in
                    self.hideHUD()
                    // handle error
                    if let error = error {

                        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                        let ok = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
                        alert.addAction(ok)
                        self.present(alert, animated: true, completion: nil)
                        print("Failed to upload image to Firebase Storage with error", error.localizedDescription)
                        return
                    }

                    // UPDATE: - Firebase 5 must now retrieve download url
                    self.showHUD()
                    storageRef.downloadURL(completion: { (downloadURL, error) in
                        self.hideHUD()
                        if let error = error {

                            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                            let ok = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
                            alert.addAction(ok)
                            self.present(alert, animated: true, completion: nil)
                            print(error.localizedDescription)
                            return
                        }

                        guard let profileImageUrl = downloadURL?.absoluteString else {
                            print("DEBUG: Profile image url is nil")
                            return
                        }
                        // user id
                        guard let uid = result?.user.uid else { return }
                        let dictionaryValues = ["username" : self.username ?? "","usertype" : self.usertype ?? "","email" : self.email ?? "", "bio":"",
                                                "profileImageUrl" : profileImageUrl,
                                                "createdAt": Int(NSDate().timeIntervalSince1970)] as [String : Any]

                        let values = [uid: dictionaryValues]
                        // save user info to database
                        REF_USERS.updateChildValues(values, withCompletionBlock: { (error, ref) in
                            if let error = error {

                                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                                let ok = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
                                alert.addAction(ok)
                                self.present(alert, animated: true, completion: nil)
                                print(error.localizedDescription)
                                return
                            }
                            self.navigateToHome()
                        })
                    })
                })
            }
            self.showAlertErrorIfNeeded(error: error)
        }

    }
    func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        self.chosenImage = info[.originalImage] as? UIImage
        self.dismiss(animated: false, completion: {
            var imageCropVC : RSKImageCropViewController!

            imageCropVC = RSKImageCropViewController(image: self.chosenImage!, cropMode: RSKImageCropMode.circle)

            imageCropVC.delegate = self
            imageCropVC.modalPresentationStyle = .fullScreen
            self.present(imageCropVC, animated: true, completion: nil)
        })
    }
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController) {
        controller.dismiss(animated: true, completion: nil)
        
    }
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect, rotationAngle: CGFloat) {
        controller.dismiss(animated: true) {
            self.btnProfile.setImage(croppedImage, for: .normal)
            self.bPhotoSelected = true
            self.btnNext.setTitle("Next", for: .normal)
        }
    }
}
