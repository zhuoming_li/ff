//
//  FinishSignupViewController.swift
//  Fanbit
//
//  Created by fanbit on 2020/12/29.
//

import UIKit
import Firebase
class FinishSignupViewController: UIViewController {

    @IBOutlet weak var btnContinueAsCreator: UIButton!
    @IBOutlet weak var btnStartBrowsing: UIButton!
    var email:String?
    var password:String?
    var username:String?
    var isFrom:String?
    var credential:AuthCredential?
    override func viewDidLoad() {
        super.viewDidLoad()
        btnContinueAsCreator.layer.cornerRadius = btnContinueAsCreator.frame.size.height/2
        btnStartBrowsing.layer.cornerRadius = btnStartBrowsing.frame.size.height/2
        btnStartBrowsing.layer.borderColor = UIColor.white.cgColor
        btnStartBrowsing.layer.borderWidth = 1
        // Do any additional setup after loading the view.
    }
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func clickContinueAsCreator(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "AddProfileViewController") as! AddProfileViewController
        if self.isFrom == "Social" {
             newViewController.credential = self.credential
        }else{
            newViewController.email = email
            newViewController.password = password
            newViewController.username = username
        }
        newViewController.usertype = CREATOR
        newViewController.isFrom = self.isFrom
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    @IBAction func clickStartBrowsing(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "AddProfileViewController") as! AddProfileViewController
        if self.isFrom == "Social" {
             newViewController.credential = self.credential
        }else{
            newViewController.email = email
            newViewController.password = password
            newViewController.username = username
        }
        newViewController.usertype = NORMAL_USER
        newViewController.isFrom = self.isFrom
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
}
