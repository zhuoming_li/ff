//
//  CreatorVerificationViewController.swift
//  Fanbit
//
//  Created by fanbit on 2020/12/29.
//

import UIKit

class CreatorVerificationViewController: UIViewController {

    @IBOutlet weak var btnYoutube: UIButton!
    @IBOutlet weak var btnTiktok: UIButton!
    @IBOutlet weak var tfBankInfo: UITextField!
    @IBOutlet weak var tfAcceptDM: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnYoutube.layer.cornerRadius = btnYoutube.frame.size.height/2
        btnTiktok.layer.cornerRadius = btnTiktok.frame.size.height/2
        // Do any additional setup after loading the view.
    }
    @IBAction func youtubeClicked(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "MainTab") as! UITabBarController
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    @IBAction func tiktokClicked(_ sender: Any) {
    }
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
