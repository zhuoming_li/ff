//
//  LoginViewController.swift
//  Fanbit
//
//  Created by fanbit on 2020/12/29.
//

import UIKit
import Firebase
import FBSDKLoginKit
import GoogleSignIn
import SwifterSwift
import AuthenticationServices
import CryptoKit
class LoginViewController: UIViewController,ASAuthorizationControllerPresentationContextProviding {

    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btnGoogle: UIButton!
    @IBOutlet weak var btnApple: UIButton!
    fileprivate var currentNonce: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        btnLogin.layer.cornerRadius = btnLogin.frame.size.height/2
        btnFacebook.layer.cornerRadius = btnFacebook.frame.size.height/2
        btnGoogle.layer.cornerRadius = btnGoogle.frame.size.height/2
        btnApple.layer.cornerRadius = btnApple.frame.size.height/2
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
        
        GIDSignIn.sharedInstance().delegate = self
        
        tfEmail.attributedPlaceholder = NSAttributedString(string: "Email",
                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        tfPassword.attributedPlaceholder = NSAttributedString(string: "Password",
                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
    }
    @available(iOS 13, *)
    private func sha256(_ input: String) -> String {
      let inputData = Data(input.utf8)
      let hashedData = SHA256.hash(data: inputData)
      let hashString = hashedData.compactMap {
        return String(format: "%02x", $0)
      }.joined()

      return hashString
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    fileprivate func loginWithSocial(credential: AuthCredential) {
        showHUD()
        FireAuth.shared.loginWith(credential: credential, completion: { (result, error) in
            if let _ = result {
                self.navigateToHome()
            } else {
                self.showErrorAlert(message: error?.localizedDescription)
            }
            self.hideHUD()
        })
    }
    func navigateToHome() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "MainTabBarController") as! MainTabBarController
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    @IBAction func forgotPassClicked(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "ResetPasswordViewController") as! ResetPasswordViewController
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    fileprivate func validateFields() -> Bool {
        if tfEmail.text?.isValidEmail == false {
            showErrorAlert(message: "Email is not valid")
            return false
        }
        if tfPassword.text?.isEmpty == true {
            showErrorAlert(message: "Password field can't be empty")
            return false
        }
        
        return true
    }
    @IBAction func loginClicked(_ sender: Any) {
        guard let email = tfEmail.text, let password = tfPassword.text, validateFields() else {
            return
        }
        showHUD()
        FireAuth.shared.loginUser(email: email, password: password) { (user, error) in
            if let _ = user {
                self.navigateToHome()
            }
            self.showAlertErrorIfNeeded(error: error)
        }
    }
    @IBAction func facebookClicked(_ sender: Any) {
        LoginManager().logIn(permissions: ["public_profile", "email"], from: self, handler: { (result, error) in
            if let error = error {
                self.showErrorAlert(message: error.localizedDescription)
            } else if result?.isCancelled == true {
                self.showErrorAlert(message: "Cancel")
            } else {
                let credential = FacebookAuthProvider.credential(withAccessToken: AccessToken.current!.tokenString)
                self.loginWithSocial(credential: credential)
            }
        })
    }
    @IBAction func googleClicked(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    @IBAction func appleClicked(_ sender: Any) {
        startSignInWithAppleFlow()
    }
    @available(iOS 13, *)
    func startSignInWithAppleFlow() {
      let nonce = randomNonceString()
      currentNonce = nonce
      let appleIDProvider = ASAuthorizationAppleIDProvider()
      let request = appleIDProvider.createRequest()
      request.requestedScopes = [.fullName, .email]
      request.nonce = sha256(nonce)

      let authorizationController = ASAuthorizationController(authorizationRequests: [request])
      authorizationController.delegate = self
      authorizationController.presentationContextProvider = self
      authorizationController.performRequests()
    }
    private func randomNonceString(length: Int = 32) -> String {
      precondition(length > 0)
      let charset: Array<Character> =
          Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
      var result = ""
      var remainingLength = length

      while remainingLength > 0 {
        let randoms: [UInt8] = (0 ..< 16).map { _ in
          var random: UInt8 = 0
          let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
          if errorCode != errSecSuccess {
            fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
          }
          return random
        }

        randoms.forEach { random in
          if remainingLength == 0 {
            return
          }

          if random < charset.count {
            result.append(charset[Int(random)])
            remainingLength -= 1
          }
        }
      }
      return result
    }
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view as! ASPresentationAnchor
    }
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - GIDSignInDelegate

extension LoginViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        if let error = error {
            showErrorAlert(message: error.localizedDescription)
            return
        }
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        self.loginWithSocial(credential: credential)
    }
}
@available(iOS 13.0, *)
extension LoginViewController: ASAuthorizationControllerDelegate {

  func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
    if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
      guard let nonce = currentNonce else {
        fatalError("Invalid state: A login callback was received, but no login request was sent.")
      }
      guard let appleIDToken = appleIDCredential.identityToken else {
        print("Unable to fetch identity token")
        return
      }
      guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
        print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
        return
      }
      // Initialize a Firebase credential.
      let credential = OAuthProvider.credential(withProviderID: "apple.com",
                                                idToken: idTokenString,
                                                rawNonce: nonce)
        self.loginWithSocial(credential: credential)
    }
  }

  func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
    // Handle error.
    print("Sign in with Apple errored: \(error)")
  }
}
