//
//  ResetPasswordViewController.swift
//  Fanbit
//
//  Created by fanbit on 2021/1/18.
//

import UIKit
import SwifterSwift
class ResetPasswordViewController: UIViewController {

    @IBOutlet weak var tfEmail: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
        tfEmail.attributedPlaceholder = NSAttributedString(string: "Email",
                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
    @IBAction func resetPasswordClicked(_ sender: Any) {
        if tfEmail.text?.isValidEmail == true {
            sendPasswordReset()
        } else {
            showErrorAlert(message: "Invalid email")
        }
    }
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController()
    }
    private func sendPasswordReset() {
        guard let email = tfEmail.text else {
            return
        }
        
        showHUD()
        FireAuth.shared.resetPassword(email: email) { (error) in
            self.hideHUD()
            if let error = error {
                self.showErrorAlert(message: error.localizedDescription)
            } else {
                self.navigationController?.popViewController()
            }
        }
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
}
