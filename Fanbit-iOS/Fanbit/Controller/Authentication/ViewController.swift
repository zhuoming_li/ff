//
//  ViewController.swift
//  Fanbit
//
//  Created by fanbit on 2020/12/29.
//

import UIKit
import Firebase
class ViewController: UIViewController {

    @IBOutlet weak var btnCreateAccount: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Auth.auth().currentUser != nil {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "MainTabBarController") as! MainTabBarController
            self.navigationController?.pushViewController(newViewController, animated: true)
        }
        

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        btnCreateAccount.layer.cornerRadius = btnCreateAccount.frame.size.height/2
        btnLogin.layer.cornerRadius = btnLogin.frame.size.height/2
        btnLogin.layer.borderWidth = 1
        btnLogin.layer.borderColor = UIColor.white.cgColor
        // Do any additional setup after loading the view.
    }

    @IBAction func creatAccountClicked(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    @IBAction func loginClicked(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    
}

