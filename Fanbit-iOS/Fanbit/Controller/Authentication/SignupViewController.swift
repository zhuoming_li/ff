//
//  SignupViewController.swift
//  Fanbit
//
//  Created by fanbit on 2020/12/29.
//

import UIKit
import Firebase
import FBSDKLoginKit
import GoogleSignIn
import SwifterSwift
import AuthenticationServices
import CryptoKit
class SignupViewController: UIViewController,ASAuthorizationControllerPresentationContextProviding {

    @IBOutlet weak var btnSignupFacebook: UIButton!
    @IBOutlet weak var btnSignupGoogle: UIButton!
    @IBOutlet weak var btnSignupApple: UIButton!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var btnSignup: UIButton!
    fileprivate var currentNonce: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        btnSignupFacebook.layer.cornerRadius = btnSignupFacebook.frame.size.height/2
        btnSignupGoogle.layer.cornerRadius = btnSignupGoogle.frame.size.height/2
        btnSignupApple.layer.cornerRadius = btnSignupApple.frame.size.height/2
        btnSignup.layer.cornerRadius = btnSignup.frame.size.height/2
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
        
        tfPassword.autocorrectionType = .no
        
        
        tfEmail.attributedPlaceholder = NSAttributedString(string: "Email",
                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        tfUsername.attributedPlaceholder = NSAttributedString(string: "Username",
                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        tfPassword.attributedPlaceholder = NSAttributedString(string: "Password",
                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
    }
    fileprivate func moveWithSocial(credential: AuthCredential) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "FinishSignupViewController") as! FinishSignupViewController
        newViewController.credential = credential
        newViewController.isFrom = "Social"
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    @IBAction func signupClicked(_ sender: Any) {
        guard let email = tfEmail.text, let password = tfPassword.text,let username = tfUsername.text, validateFields() else {
                return
        }
        UserService.shared.checkEmailWasAlreadyTaken(email: email) { bChecked in
            if bChecked{
                self.showErrorAlert(message: "This email was taken already by another user, please use another one")
            }else{
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "FinishSignupViewController") as! FinishSignupViewController
                newViewController.email = email
                newViewController.password = password
                newViewController.username = username
                newViewController.isFrom = "Email"
                self.navigationController?.pushViewController(newViewController, animated: true)
            }
        }
        
    }
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func googlesignupClicked(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
       
    }
    @IBAction func facebookSignupClicked(_ sender: Any) {
        LoginManager().logIn(permissions: ["public_profile", "email"], from: self, handler: { (result, error) in
            if let error = error {
//                self.showErrorAlert(message: error.localizedDescription)
            } else if result?.isCancelled == true {
                self.showErrorAlert(message: "Cancel")
            } else {
                let credential = FacebookAuthProvider.credential(withAccessToken: AccessToken.current!.tokenString)
                self.moveWithSocial(credential: credential)
            }
        })
    }
    @IBAction func applesignupClicked(_ sender: Any) {
        startSignInWithAppleFlow()
    }
    fileprivate func validateFields() -> Bool {
        if tfEmail.text?.isValidEmail == false {
            showErrorAlert(message: "Email is not valid")
            return false
        }
        
        if tfPassword.text?.isEmpty == true  {
            showErrorAlert(message: "Password field can't be empty")
            return false
        }
        if tfUsername.text?.isEmpty == true  {
            showErrorAlert(message: "Username field can't be empty")
            return false
        }
        return true
    }
    @available(iOS 13, *)
    private func sha256(_ input: String) -> String {
      let inputData = Data(input.utf8)
      let hashedData = SHA256.hash(data: inputData)
      let hashString = hashedData.compactMap {
        return String(format: "%02x", $0)
      }.joined()

      return hashString
    }
    @available(iOS 13, *)
    func startSignInWithAppleFlow() {
      let nonce = randomNonceString()
      currentNonce = nonce
      let appleIDProvider = ASAuthorizationAppleIDProvider()
      let request = appleIDProvider.createRequest()
      request.requestedScopes = [.fullName, .email]
      request.nonce = sha256(nonce)

      let authorizationController = ASAuthorizationController(authorizationRequests: [request])
      authorizationController.delegate = self
      authorizationController.presentationContextProvider = self
      authorizationController.performRequests()
    }
    private func randomNonceString(length: Int = 32) -> String {
      precondition(length > 0)
      let charset: Array<Character> =
          Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
      var result = ""
      var remainingLength = length

      while remainingLength > 0 {
        let randoms: [UInt8] = (0 ..< 16).map { _ in
          var random: UInt8 = 0
          let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
          if errorCode != errSecSuccess {
            fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
          }
          return random
        }

        randoms.forEach { random in
          if remainingLength == 0 {
            return
          }

          if random < charset.count {
            result.append(charset[Int(random)])
            remainingLength -= 1
          }
        }
      }
      return result
    }
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view as! ASPresentationAnchor
    }
}
extension SignupViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        if let error = error {
            showErrorAlert(message: error.localizedDescription)
            return
        }
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        self.moveWithSocial(credential: credential)
    }
}
@available(iOS 13.0, *)
extension SignupViewController: ASAuthorizationControllerDelegate {

  func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
    if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
      guard let nonce = currentNonce else {
        fatalError("Invalid state: A login callback was received, but no login request was sent.")
      }
      guard let appleIDToken = appleIDCredential.identityToken else {
        print("Unable to fetch identity token")
        return
      }
      guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
        print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
        return
      }
      // Initialize a Firebase credential.
      let credential = OAuthProvider.credential(withProviderID: "apple.com",
                                                idToken: idTokenString,
                                                rawNonce: nonce)
        self.moveWithSocial(credential: credential)
    }
  }

  func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
    // Handle error.
    print("Sign in with Apple errored: \(error)")
  }
}
