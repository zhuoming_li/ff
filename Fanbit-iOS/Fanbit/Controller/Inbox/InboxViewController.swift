//
//  InboxViewController.swift
//  Fanbit
//
//  Created by fanbit on 2020/12/29.
//

import UIKit
import Firebase
class InboxViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableview: UITableView!
    var notifications:[Notification] = []
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchNotifications()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    func fetchNotifications(){
        NotificationService.shared.fetchNotifications(uid: (Auth.auth().currentUser?.uid as? String)!) { notifications in
            self.notifications = notifications
            self.tableview.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notifications.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 71
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InboxTableViewCell", for: indexPath) as! InboxTableViewCell
        cell.initwithdata(notification:self.notifications[indexPath.row])
        cell.btnConfirm.tag = indexPath.row
        cell.btnConfirm.addTarget(self, action: #selector(confirmClicked(_ :)), for: .touchUpInside)
        return cell
    }
    @objc func confirmClicked(_ sendor : UIButton){
        let notification = notifications[sendor.tag]
        ProjectService.shared.fetchProjectByID(projectID: notification.projectID!) { project in
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "ProjectPageViewController") as! ProjectPageViewController
            newViewController.project = project
            
            if notification.type == NotificationType.submitted {
                newViewController.isFrom = "Submitted"
            }else if notification.type == NotificationType.liked{
                newViewController.isFrom = "Liked"
            }else{
                newViewController.isFrom = "Commented"
            }
            self.navigationController?.pushViewController(newViewController, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let newViewController = storyBoard.instantiateViewController(withIdentifier: "ProjectPageViewController") as! ProjectPageViewController
//        self.navigationController?.pushViewController(newViewController, animated: true)
    }
}
