//
//  CommentsViewController.swift
//  Fanbit
//
//  Created by fanbit on 2021/1/15.
//

import UIKit
import Firebase
class CommentsViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var ivCurrentUserProfile: UIImageView!
    @IBOutlet weak var tfComment: UITextField!
    @IBOutlet weak var tableview: UITableView!
    var project:Project!
    var comments:[Comment] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchComments()
        
        UserService.shared.fetchUser(uid: Auth.auth().currentUser!.uid) { user in
            self.ivCurrentUserProfile.sd_setImage(with: user.profileImageUrl, completed: nil)
        }
        // Do any additional setup after loading the view.
    }
    func fetchComments() {
        ProjectService.shared.fetchCommentsByProject(projectID: self.project.projectID) { comments in
            self.comments = comments
            self.tableview.reloadData()
        }
    }
    @IBAction func sendClicked(_ sender: Any) {
        if tfComment.text == "" {
            showErrorAlert(message: "Fill comments first")
            return
        }
        ProjectService.shared.commentProject(projectID: self.project.projectID, comment: self.tfComment.text!) { err, ref in
            if err == nil{
                self.tfComment.text = ""
                self.fetchComments()
            }
        }
        guard let currentId = Auth.auth().currentUser?.uid else { return }
        ActivityService.shared.uploadActivity(userID: currentId, type: ActivityType.commentedProject, projectID: self.project.projectID)
    }
    @IBAction func backClickee(_ sender: Any) {
        self.navigationController?.popViewController()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.comments.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 112
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell", for: indexPath) as! CommentTableViewCell
        cell.initwithdata(comment:self.comments[indexPath.row])
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

}
