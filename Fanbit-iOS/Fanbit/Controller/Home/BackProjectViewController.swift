//
//  BackProjectViewController.swift
//  Fanbit
//
//  Created by fanbit on 2020/12/29.
//

import UIKit

class BackProjectViewController: UIViewController {

    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var btnPledge: UIButton!
    @IBOutlet weak var btnApplePay: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        btnPlus.layer.cornerRadius = btnPlus.frame.size.height/2
        btnMinus.layer.cornerRadius = btnMinus.frame.size.height/2
        btnPledge.layer.cornerRadius = btnPledge.frame.size.height/2
        btnApplePay.layer.cornerRadius = btnApplePay.frame.size.height/2
        // Do any additional setup after loading the view.
    }
    
    @IBAction func applepayClicked(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "PaymentSuccessViewController") as! PaymentSuccessViewController
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }


}
