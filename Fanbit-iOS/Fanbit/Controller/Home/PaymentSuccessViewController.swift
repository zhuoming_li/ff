//
//  PaymentSuccessViewController.swift
//  Fanbit
//
//  Created by fanbit on 2020/12/29.
//

import UIKit

class PaymentSuccessViewController: UIViewController {

    @IBOutlet weak var btnShareProject: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnShareProject.layer.cornerRadius = btnShareProject.frame.size.height/2
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
