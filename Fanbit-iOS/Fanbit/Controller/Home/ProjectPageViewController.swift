//
//  ProjectPageViewController.swift
//  Fanbit
//
//  Created by fanbit on 2020/12/29.
//

import UIKit
import Firebase
import PassKit
class ProjectPageViewController: UIViewController ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UICollectionViewDelegate{

    
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var ivPhoto: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var btnHomePage: UIButton!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var viewPaymentSuccess: UIView!
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var ivProfile: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblProjectName: UILabel!
    
    @IBOutlet weak var lblCreatorName: UILabel!
    @IBOutlet weak var lblUsername1: UILabel!
    @IBOutlet weak var lblFan: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblBackerCount: UILabel!
    @IBOutlet weak var lblFundsRaised: UILabel!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var lblBackupPrice: UILabel!
    @IBOutlet weak var successBackup: UILabel!
    var project:Project!
    var creator:User!
    var isFrom:String?
    var price = 0.5
    private var projects = [Project]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ivPhoto.layer.cornerRadius = 20
        btnBack.layer.cornerRadius = btnBack.frame.size.height/2
        btnComment.layer.cornerRadius = btnComment.frame.size.height/2
        btnComment.layer.borderWidth = 1
        btnComment.layer.borderColor = UIColor.white.cgColor
        
        btnHomePage.layer.cornerRadius = btnHomePage.frame.size.height/2
        btnHomePage.layer.borderWidth = 1
        btnHomePage.layer.borderColor = UIColor.white.cgColor
        viewContainer.layer.cornerRadius = 20
        viewBottom.isHidden = true
        viewPaymentSuccess.isHidden =  true
        
        initialize()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        ivProfile.isUserInteractionEnabled = true
        ivProfile.addGestureRecognizer(tapGestureRecognizer)
        fetchActiveProjects()
        
        
    }
    func fetchActiveProjects() {
        ProjectService.shared.fetchActiveProjects(completion: ) { projects in
            self.projects = projects
            self.collectionview.reloadData()
        }
    }
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        newViewController.user = self.creator
        newViewController.bCurrentUser = false
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    func initialize(){
        let creatorID = project.creatorSelected
        UserService.shared.fetchUser(uid: creatorID) { user in
            self.creator = user
            self.ivProfile.sd_setImage(with: user.profileImageUrl, completed: nil)
            self.lblProjectName.text = self.project.projectName
            self.lblCreatorName.text = String.init(format: "Sponsored by %@",  self.creator.username)
            self.lblUsername1.text = user.username
        }
        let makerID = project.maker
        UserService.shared.fetchUser(uid: makerID) { user in
            self.lblFan.text = user.username
        }
        self.ivPhoto.sd_setImage(with:URL(string: project.mediaURLs.first), completed: nil)
        self.lblDate.text =  project.timestamp.dateString()
        self.lblDescription.text = self.project.description
        
        ProjectService.shared.checkLikedProject(projectID: self.project.projectID) { bLiked in
            if bLiked {
                self.btnLike.setImage(UIImage.init(named: "heart_fill"), for: .normal)
            } else {
                self.btnLike.setImage(UIImage.init(named: "heart"), for: .normal)
            }
        }
        
        if self.isFrom == "Home" {
            if UserDefaults.standard.value(forKey: "type") as! String == CREATOR {
                btnBack.isHidden = true
            }else{
                 ProjectService.shared.checkBacked(projectID: self.project.projectID, completion: { (backed) in
                    if backed{
                        self.btnBack.isHidden = false
                        self.btnBack.setTitle("Already Backed this project", for: .normal)
                        self.btnBack.isUserInteractionEnabled = false
                    } else{
                        self.btnBack.isHidden = false
                        self.btnBack.setTitle("Back this project", for: .normal)
                    }
                })
                
            }
        } else if self.isFrom == "Submitted" {
            btnBack.setTitle("Accept this project", for: .normal)
        } else {
            btnBack.setTitle("Back this project", for: .normal)
        }
        self.lblBackupPrice.text = String(self.price)
        self.lblFundsRaised.text = String(self.project.fundsRaised)
        self.lblBackerCount.text = String(self.project.numberOfBackers)
        if self.project.status == PROJECT_STATUS.Active.rawValue {
            self.lblStatus.text = "Accepted"
        }else if self.project.status == PROJECT_STATUS.Delivered.rawValue {
            self.lblStatus.text = "Delivered"
        }
        
        
    }
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func clickBackProject(_ sender: Any) {
        if self.isFrom == "Home" {
            viewBottom.isHidden = false
        }else if self.isFrom == "Submitted" {
            guard let currentId = Auth.auth().currentUser?.uid else { return }
            ProjectService.shared.updateProject(projectID: self.project.projectID, values: ["status":  PROJECT_STATUS.Active.rawValue] as [String : Any]) { (err, ref) in
                self.btnBack.setTitle("Accepted", for: .normal)
                self.isFrom = "Accepted"
            }
            NotificationService.shared.updateNotification(uid: currentId, projectID: self.project.projectID, values: ["status":NotificationStatus.accepted.rawValue]) { (err, ref) in
                
            }
            ActivityService.shared.uploadActivity(userID: currentId, type: ActivityType.acceptedProject, projectID: self.project.projectID)
        }
    }
    
    @IBAction func likeClicked(_ sender: Any) {
        
        ProjectService.shared.checkLikedProject(projectID: self.project.projectID) { bLiked in
            if bLiked {
                ProjectService.shared.unlikeProject(projectID: self.project.projectID) { (err, ref) in
                    self.btnLike.setImage(UIImage.init(named: "heart"), for: .normal)
                }
            } else {
                ProjectService.shared.likeProject(projectID: self.project.projectID) { (err, ref) in
                    self.btnLike.setImage(UIImage.init(named: "heart_fill"), for: .normal)
                }
            }
        }
        guard let currentId = Auth.auth().currentUser?.uid else { return }
        ProjectService.shared.likeProject(projectID: self.project.projectID) { err, ref in
            if err == nil{
                self.showSuccessAlert(message: "Liked this project")
                
            }
        }
        ActivityService.shared.uploadActivity(userID: currentId, type: ActivityType.likedProject, projectID: self.project.projectID)
    }
    @IBAction func commentClicked(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "CommentsViewController") as! CommentsViewController
        newViewController.project = self.project
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    @IBAction func homePageClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func closeBottomClicked(_ sender: Any) {
        viewBottom.isHidden = true
    }
    @IBAction func applePayClicked(_ sender: Any) {
        viewBottom.isHidden = true
        viewPaymentSuccess.isHidden = false
        self.successBackup.text = String.init(format: "You have successfully backed %@", self.project.projectName)
        guard let currentId = Auth.auth().currentUser?.uid else { return }
        ActivityService.shared.uploadActivity(userID: currentId, type: ActivityType.backedProject, projectID: self.project.projectID)
        
        ProjectService.shared.backProject(projectID: self.project.projectID, price: 10.5) { (err, ref) in
            
        }
        return
        
        
//        viewBottom.isHidden = true
//        let paymentItem = PKPaymentSummaryItem.init(label: self.project.projectName, amount: NSDecimalNumber(value: self.price))
//        let paymentNetworks = [PKPaymentNetwork.amex, .discover, .masterCard, .visa]
//        if PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetworks) {
//            let request = PKPaymentRequest()
//                request.currencyCode = "USD" // 1
//                request.countryCode = "US" // 2
//                request.merchantIdentifier = "merchant.com.pranavwadhwa.Shoe-Store" // 3
//                request.merchantCapabilities = PKMerchantCapability.capability3DS // 4
//                request.supportedNetworks = paymentNetworks // 5
//                request.paymentSummaryItems = [paymentItem] // 6
//
//                guard let paymentVC = PKPaymentAuthorizationViewController(paymentRequest: request) else {
//                    displayDefaultAlert(title: "Error", message: "Unable to present Apple Pay authorization.")
//                    return
//                }
//                paymentVC.delegate = self
//                self.present(paymentVC, animated: true, completion: nil)
//
//
//        } else {
//            displayDefaultAlert(title: "Error", message: "Unable to make Apple Pay transaction.")
//        }
        
    }
    @IBAction func paymentCloseClicked(_ sender: Any) {
        viewPaymentSuccess.isHidden = true
        self.navigationController?.popViewController()
    }
    @IBAction func shareClicked(_ sender: Any) {
    }
    
    @IBAction func priceDownClicked(_ sender: Any) {
        if self.price == 0.5 {
            return
        }
        self.price = self.price - 0.5
        self.lblBackupPrice.text = String(self.price)
    }
    @IBAction func priceUpClicked(_ sender: Any) {
        if self.price == 100 {
            return
        }
        self.price = self.price + 0.5
        self.lblBackupPrice.text = String(self.price)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.projects.count
    }
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProjectCollectionViewCell", for: indexPath as IndexPath) as! ProjectCollectionViewCell
            cell.initwithdata(project: self.projects[indexPath.row])
            return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "ProjectPageViewController") as! ProjectPageViewController
            newViewController.project = self.projects[indexPath.row]
            newViewController.isFrom = "Home"
            self.navigationController?.pushViewController(newViewController, animated: true)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
            let cellWidth = collectionView.frame.size.height - 10
            let cellHeight = collectionView.frame.size.height + 50
            return CGSize(width: cellWidth, height: cellHeight)
        
    }
    func displayDefaultAlert(title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
       let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
}
extension ProjectPageViewController: PKPaymentAuthorizationViewControllerDelegate {
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        dismiss(animated: true, completion: nil)

    }
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        dismiss(animated: true, completion: nil)
        viewPaymentSuccess.isHidden = false
        displayDefaultAlert(title: "Success!", message: "The Apple Pay transaction was complete.")
    }
}
