//
//  HomeViewController.swift
//  Fanbit
//
//  Created by fanbit on 2020/12/29.
//

import UIKit


class HomeViewController: UIViewController,UITableViewDelegate, UITableViewDataSource,UITabBarControllerDelegate{

    @IBOutlet weak var tableview: UITableView! // tableview for projects
    private var creators = [User]()
    private var projects = [Project]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.delegate = self
        fetchCreators()
        checkUserType()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchActiveProjects()
    }
    func checkUserType(){
        UserService.shared.checkUserType{ type in
            print(type)
            UserDefaults.standard.setValue(type, forKey: "type")
        }
    }
    func fetchCreators() {
        UserService.shared.fetchCreators { users in
           self.creators = users
           self.tableview.reloadData()
        }
    }
    func fetchActiveProjects() {
        ProjectService.shared.fetchActiveProjects(completion: ) { projects in
            self.projects = projects
            self.tableview.reloadData()
        }
    }

    // make a cell for each cell index path
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
         let headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 60))
        headerView.backgroundColor = .clear
        let label = UILabel.init(frame: CGRect(x: 0, y: 20, width: tableView.frame.size.width, height: 20))
        if section == 0 {
            label.text = "Creators"
        }else{
            label.text = "Projects"
        }
        label.textColor = .white
         headerView.addSubview(label)
         return headerView
     }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else{
            return self.projects.count
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 130
        }else{
            return 370
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeHeadTableViewCell", for: indexPath) as! HomeHeadTableViewCell
            cell.delegate = self
            cell.initwithdata(users:self.creators)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as! HomeTableViewCell
            cell.initwithdata(project:self.projects[indexPath.row])
            cell.btnLike.tag = indexPath.row
            cell.btnLike.addTarget(self, action: #selector(likeClicked(_ :)), for: .touchUpInside)
            cell.btnComment.tag = indexPath.row
            cell.btnComment.addTarget(self, action: #selector(commentClicked(_ :)), for: .touchUpInside)
            cell.btnAddComment.tag = indexPath.row
            cell.btnAddComment.addTarget(self, action: #selector(commentClicked(_ :)), for: .touchUpInside)
             return cell
        }
    }
    @objc func commentClicked(_ sendor : UIButton){
        let project = self.projects[sendor.tag]
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "CommentsViewController") as! CommentsViewController
        newViewController.project = project
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    @objc func likeClicked(_ sendor : UIButton){
        let project = self.projects[sendor.tag]
        ProjectService.shared.checkLikedProject(projectID: project.projectID) { bLiked in
            if bLiked{
                ProjectService.shared.unlikeProject(projectID: project.projectID) { (err, ref) in
                    self.tableview.reloadData()
                }
            }else{
                ProjectService.shared.likeProject(projectID: project.projectID) { (err, ref) in
                    self.tableview.reloadData()
                }
            }
            
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "ProjectPageViewController") as! ProjectPageViewController
            newViewController.project = self.projects[indexPath.row]
            newViewController.isFrom = "Home"
            self.navigationController?.pushViewController(newViewController, animated: true)
        }
       
    }
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if tabBarController.selectedIndex == 2 {
            if UserDefaults.standard.value(forKey: "type") as! String == CREATOR {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "UploadMediaViewController") as! UploadMediaViewController
                 self.navigationController?.pushViewController(newViewController, animated: false)
            }else{
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "SelectCreatorViewController") as! SelectCreatorViewController
                self.navigationController?.pushViewController(newViewController, animated: false)
            }
        }
    }
}
extension HomeViewController: CreatorSelectDelegate {
    func selectCreator(nIndex:Int) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "CreatorPageViewController") as! CreatorPageViewController
        newViewController.creatorSelected = self.creators[nIndex]
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
}
