//
//  CreatorPageViewController.swift
//  Fanbit
//
//  Created by fanbit on 2020/12/29.
//

import UIKit

class CreatorPageViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var ivPhoto: UIImageView!
    @IBOutlet weak var btnMessage: UIButton!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var lblName: UILabel!
    var creatorSelected : User!
    private var projects = [Project]()
    override func viewDidLoad() {
        super.viewDidLoad()
        btnMessage.layer.cornerRadius = btnMessage.frame.size.height/2
        fetchProjectSubmittedToThisCreator()
        lblName.text = creatorSelected.username
        
        ivPhoto.sd_setImage(with: creatorSelected.profileImageUrl, completed: nil)
        // Do any additional setup after loading the view.
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        ivPhoto.isUserInteractionEnabled = true
        ivPhoto.addGestureRecognizer(tapGestureRecognizer)
    }
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        newViewController.user = creatorSelected
        newViewController.bCurrentUser = false
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    func fetchProjectSubmittedToThisCreator(){
        ProjectService.shared.fetchProjectsSubmittedToCreator(uid: creatorSelected.uid) { projects in
            self.projects = projects
            self.tableview.reloadData()
        }
    }
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.projects.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 370
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as! HomeTableViewCell
        cell.initwithdata(project:self.projects[indexPath.row])
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let newViewController = storyBoard.instantiateViewController(withIdentifier: "ProjectPageViewController") as! ProjectPageViewController
//        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    

}
