//
//  SettingViewController.swift
//  Fanbit
//
//  Created by fanbit on 2021/1/10.
//

import UIKit
import RSKImageCropper
class SettingViewController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate, RSKImageCropViewControllerDelegate{

    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblBio: UILabel!
    lazy var imagePicker = UIImagePickerController()
    var user:User!
    var bPhotoUpdated = false
    var chosenImage: UIImage?
    override func viewDidLoad() {
        super.viewDidLoad()
                
        self.btnProfile.sd_setImage(with: self.user.profileImageUrl, for: .normal, completed: nil)
        self.lblUsername.text = self.user.username
        self.lblBio.text = self.user.bio
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController()
    }
    @IBAction func profileClicked(_ sender: UIButton) {
        imagePicker.delegate = self
        let alert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
            let takePhoto = UIAlertAction.init(
                title: "Take Photo",
                style: .default,
                handler: { (UIAlertAction) -> Void in
                    self.imagePicker.sourceType = .camera
                    self.present(self.imagePicker, animated: true, completion: nil)
            })
            let choosePhotoAction = UIAlertAction.init(
                title: "Choose From Library",
                style: .default,
                handler: { (UIAlertAction) -> Void in
                    self.imagePicker.sourceType = .photoLibrary
                    self.present(self.imagePicker, animated: true, completion: nil)
            })
            let cancel = UIAlertAction.init(
                title: "Cancel",
                style: .cancel,
                handler: nil)
            alert.addAction(takePhoto)
            alert.addAction(choosePhotoAction)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
    }
    @IBAction func usernameChangeClicked(_ sender: Any) {
        alertWithTextField(title: "Update Username", message: "Please change username", placeholder: self.lblUsername.text) { result in
            print(result)
            if result != ""{
                self.lblUsername.text = result
            }
        }

    }
    @IBAction func bioChangeClicked(_ sender: Any) {
        alertWithTextField(title: "Update Bio", message: "Please change bio", placeholder: self.lblBio.text) { result in
            print(result)
            if result != ""{
                self.lblBio.text = result
            }
        }
    }
    @IBAction func updateClicked(_ sender: Any) {
        if bPhotoUpdated {
            UserService.shared.updateProfileImage(image: self.btnProfile.currentImage!) { url in
                self.user.profileImageUrl = url
                self.user.username = self.lblUsername.text!
                self.user.bio = self.lblBio.text!
                UserService.shared.saveUserData(user: self.user) { (error, ref) in
                    self.showSuccessAlert(message: "Updated Successfully!")
                }
            }
        }else{
            self.user.username = self.lblUsername.text!
            self.user.bio = self.lblBio.text!
            UserService.shared.saveUserData(user: self.user) { (error, ref) in
                self.showSuccessAlert(message: "Updated Successfully!")
            }
        }
    }
    public func alertWithTextField(title: String? = nil, message: String? = nil, placeholder: String? = nil, completion: @escaping ((String) -> Void) = { _ in }) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addTextField() { newTextField in
            newTextField.placeholder = placeholder
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in completion("") })
        alert.addAction(UIAlertAction(title: "Ok", style: .default) { action in
            if
                let textFields = alert.textFields,
                let tf = textFields.first,
                let result = tf.text
            { completion(result) }
            else
            { completion("") }
        })
        navigationController?.present(alert, animated: true)
    }
    @IBAction func logoutClicked(_ sender: Any) {
        FireAuth.shared.signOut {
            self.navigationController?.popToRootViewController(animated: true)
        } failure: { error in
            self.showErrorAlert(message: error.localizedDescription)
        }

    }
    func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        self.chosenImage = info[.originalImage] as? UIImage
        self.dismiss(animated: false, completion: {
            var imageCropVC : RSKImageCropViewController!

            imageCropVC = RSKImageCropViewController(image: self.chosenImage!, cropMode: RSKImageCropMode.circle)

            imageCropVC.delegate = self
            imageCropVC.modalPresentationStyle = .fullScreen
            self.present(imageCropVC, animated: true, completion: nil)
        })
    }
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController) {
        controller.dismiss(animated: true, completion: nil)
        
    }
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect, rotationAngle: CGFloat) {
        controller.dismiss(animated: true) {
            self.btnProfile.setImage(croppedImage, for: .normal)
            self.bPhotoUpdated = true
        }
    }
}
