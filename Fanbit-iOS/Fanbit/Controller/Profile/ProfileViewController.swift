//
//  ProfileViewController.swift
//  Fanbit
//
//  Created by fanbit on 2020/12/29.
//

import UIKit
import Firebase
class ProfileViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    @IBOutlet weak var ivProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var projectCollectionview: UICollectionView!
    @IBOutlet weak var creatorCollectionview: UICollectionView!
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblBio: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnSetting: UIButton!
    
    private var creators = [User]()
    private var projects = [Project]()
    var bCurrentUser = true
    var user:User!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if bCurrentUser { // user is on his profile page
            btnFollow.isHidden = true
            btnSetting.isHidden = false
            UserService.shared.fetchUser(uid: Auth.auth().currentUser!.uid) { user in
                self.user = user
                self.ivProfile.sd_setImage(with: self.user.profileImageUrl, completed: nil)
                self.lblName.text = self.user.username
                self.lblBio.text = self.user.bio
                
                self.fetchProjectsStartedByUser()
                self.fetchFollowingUsers()
            }
            btnBack.isHidden = true
        }else{ // user is on the other user's profile page
            btnFollow.isHidden = false
            btnSetting.isHidden = true
            self.ivProfile.sd_setImage(with: self.user.profileImageUrl, completed: nil)
            self.lblName.text = self.user.username
            self.fetchProjectsStartedByUser()
            self.fetchFollowingUsers()
            btnBack.isHidden = false
            UserService.shared.checkIfUserIsFollowed(uid: self.user.uid) { bFollowing in
                if bFollowing{ // already followed
                    self.btnFollow.setTitle("Followed", for: .normal)
                    self.btnFollow.isUserInteractionEnabled = false
                }else{ // not followed yet
                    self.btnFollow.setTitle("Follow", for: .normal)
                    self.btnFollow.isUserInteractionEnabled = true
                }
            }
        }
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    func fetchFollowingUsers(){
        UserService.shared.fetchFollowingUsers(uid: self.user.uid) { users in
            self.creators = users
            self.creatorCollectionview.reloadData()
        }
    }
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController()
    }
    
    func fetchProjectsStartedByUser(){
        ProjectService.shared.fetchProjectsStartedByUser(uid: self.user.uid) { projects in
            self.projects = projects
            self.projectCollectionview.reloadData()
        }
    }
    @IBAction func followClicked(_ sender: Any) {
        UserService.shared.followUser(uid: self.user.uid) { (err, ref) in
            self.btnFollow.setTitle("Followed", for: .normal)
            self.btnFollow.isUserInteractionEnabled = false
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == projectCollectionview {
            return self.projects.count
        }else{
            return self.creators.count
        }
        
    }
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == projectCollectionview {
            // get a reference to our storyboard cell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProjectCollectionViewCell", for: indexPath as IndexPath) as! ProjectCollectionViewCell
            cell.initwithdata(project:self.projects[indexPath.row])
            return cell
        } else {
            // get a reference to our storyboard cell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath as IndexPath) as! HomeCollectionViewCell
            cell.initwithdata(user:self.creators[indexPath.row])
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == projectCollectionview {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "ProjectPageViewController") as! ProjectPageViewController
            newViewController.project = self.projects[indexPath.row]
            self.navigationController?.pushViewController(newViewController, animated: true)
        }else{
            // handle tap events
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "CreatorPageViewController") as! CreatorPageViewController
            newViewController.creatorSelected = creators[indexPath.row]
            self.navigationController?.pushViewController(newViewController, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == projectCollectionview {
            let cellWidth = collectionView.frame.size.height - 10
            let cellHeight = collectionView.frame.size.height + 50
            return CGSize(width: cellWidth, height: cellHeight)
        }else{
            let cellWidth = collectionView.frame.size.height - 10
            let cellHeight = collectionView.frame.size.height
            return CGSize(width: cellWidth, height: cellHeight)
        }
    }

    @IBAction func settingClicked(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "SettingViewController") as! SettingViewController
        newViewController.user = self.user
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
}
