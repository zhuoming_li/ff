//
//  ActivityViewController.swift
//  Fanbit
//
//  Created by fanbit on 2020/12/29.
//

import UIKit
import Firebase

class ActivityViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableview: UITableView!
    private var activities = [Activity]()
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchActivities()
        // Do any additional setup after loading the view.
    }
    func fetchActivities(){
        guard let uid = Auth.auth().currentUser?.uid else { return }
        ActivityService.shared.fetchActivities(uid: uid) { (activities) in
            self.activities = activities
            self.tableview.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.activities.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 389
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityTableViewCell", for: indexPath) as! ActivityTableViewCell
        cell.initwithdata(activity:self.activities[indexPath.row])
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }

}
