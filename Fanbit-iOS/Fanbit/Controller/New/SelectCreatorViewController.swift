//
//  SelectCreatorViewController.swift
//  Fanbit
//
//  Created by fanbit on 2021/1/8.
//

import UIKit

class SelectCreatorViewController: UIViewController ,UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var tfSearch: UITextField!
    private var creators = [User]()
    private var creatorsSearched = [User]()
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    var bSearch = false
    var nSelectedIndex = 0
    var userSelected:User!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnNext.layer.cornerRadius = btnNext.frame.size.height/2
        
        
        fetchCreators()
        // Do any additional setup after loading the view.
    }
    @IBAction func searchClicked(_ sender: Any) {
        if tfSearch.text == "" {
            return
        }
        tfSearch.resignFirstResponder()
        bSearch = true
        for user in creators{
            if user.username.contains(tfSearch.text!) {
                creatorsSearched.append(user)
            }
        }
        tableview.reloadData()
    }
    @IBAction func searchFieldCancelClicked(_ sender: Any) {
        tfSearch.text = ""
        tfSearch.resignFirstResponder()
        bSearch = false
        nSelectedIndex = 0
        userSelected = creators.first
        tableview.reloadData()
    }
    
    func fetchCreators() {
        UserService.shared.fetchCreators { users in
           self.creators = users
           self.userSelected = users.first
           self.tableview.reloadData()
            if self.creators.count == 0 {
                self.btnSearch.isUserInteractionEnabled = false
                self.btnCancel.isUserInteractionEnabled = false
                self.tfSearch.isUserInteractionEnabled = false
            }
        }
        
    }
    @IBAction func nextClicked(_ sender: Any) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "UploadMediaViewController") as! UploadMediaViewController
        newViewController.creatorSelected = userSelected
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    
    @IBAction func backClicked(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "MainTabBarController") as! MainTabBarController
        self.navigationController?.pushViewController(newViewController, animated: false)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if bSearch {
            return self.creatorsSearched.count
        }else{
            return self.creators.count
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 124
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectCreatorTableViewCell", for: indexPath) as! SelectCreatorTableViewCell
        if bSearch {
            cell.initwithdata(user:self.creatorsSearched[indexPath.row])
        }else{
            cell.initwithdata(user:self.creators[indexPath.row])
        }
        if nSelectedIndex == indexPath.row {
            cell.ivBack.image = UIImage.init(named: "Ellipse_Sel")
        }else{
            cell.ivBack.image = UIImage.init(named: "Ellipse")
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        nSelectedIndex = indexPath.row
        if bSearch {
            userSelected = self.creatorsSearched[indexPath.row]
        }else{
            userSelected = self.creators[indexPath.row]
        }
        tableview.reloadData()
    }

}
