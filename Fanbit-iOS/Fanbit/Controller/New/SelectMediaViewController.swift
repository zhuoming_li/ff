//
//  SelectMediaViewController.swift
//  Fanbit
//
//  Created by fanbit on 2021/1/8.
//

import UIKit
import PhotosUI
protocol SelectMediaDelegate {
    func setUploadedMediaURLs(urls:[String])
    
}
class SelectMediaViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {

    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var sgType: UISegmentedControl!
    let buttonBar = UIView()
    var currentImageAddedCount = 0
    var imageAssets = [PHAsset]()
    var videoAssets = [PHAsset]()
    var selectedImageAssets = [PHAsset]()
    var selectedVideoAssets = [PHAsset]()
    var selectedImageArray = [String]()
    var selectedVideoArray = [String]()
    var delegate : SelectMediaDelegate?
    let nMaxCount = 5
    var mediaURLs:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSegmentedControl()
        getAllImagesFromGallary()
        getAllVideosFromGallery()
    }
    @IBAction func nextClicked(_ sender: Any) {
        if selectedImageArray.count == 0 {
            showErrorAlert(message: "Select Images first!")
            return
        }
        var filename = String.init(format: "%d", Int(NSDate().timeIntervalSince1970))
        self.showHUD()
        for id in 0...selectedImageArray.count - 1{
            let index = selectedImageArray[id]
            selectedImageAssets.append(imageAssets[Int(index)!])
            
            let image =  self.getAssetThumbnail(asset: imageAssets[Int(index)!])
            guard let uploadData = image!.jpegData(compressionQuality: 0.1) else { return }
            filename = String.init(format: "%@_%d", filename, id)
            let storageRef = STORAGE_PROJECT_IMAGES.child(filename)
            
            storageRef.putData(uploadData, metadata: nil, completion: { (metadata, error) in
                
                // handle error
                if let error = error {
                    self.hideHUD()
                    let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                    let ok = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
                    alert.addAction(ok)
                    self.present(alert, animated: true, completion: nil)
                    print("Failed to upload image to Firebase Storage with error", error.localizedDescription)
                    return
                }

                // UPDATE: - Firebase 5 must now retrieve download url
                storageRef.downloadURL(completion: { (downloadURL, error) in
                    
                    if let error = error {
                        self.hideHUD()
                        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                        let ok = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
                        alert.addAction(ok)
                        self.present(alert, animated: true, completion: nil)
                        print(error.localizedDescription)
                        return
                    }

                    guard let profileImageUrl = downloadURL?.absoluteString else {
                        self.hideHUD()
                        print("DEBUG: Profile image url is nil")
                        return
                    }
                    self.mediaURLs.append(profileImageUrl)
                    if self.mediaURLs.count == self.selectedImageArray.count {
                        self.hideHUD()
                        self.delegate?.setUploadedMediaURLs(urls: self.mediaURLs)
                                               
                        
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "MediaViewController") as! MediaViewController
                        newViewController.imageAssets = self.selectedImageAssets
                        self.navigationController?.pushViewController(newViewController, animated: true)
                    }
                })
            })

        }
    }
    func getAllVideosFromGallery(){
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.denied  || PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.restricted {
            //Access Denited
        }else{
            PHPhotoLibrary.requestAuthorization { (status) in
                let fetchOptions = PHFetchOptions()
                fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
                let imgArr = PHAsset.fetchAssets(with: .video, options: fetchOptions) as PHFetchResult
                for i in 0 ..< imgArr.count {
                    self.videoAssets.append(imgArr[i])
                    print(self.videoAssets[i])
                }
                print(self.videoAssets.count)
                
            }

        }
    }
    func getAllImagesFromGallary() {
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.denied  || PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.restricted {
            //Access Denited
        }else{
            PHPhotoLibrary.requestAuthorization { (status) in
                let fetchOptions = PHFetchOptions()
                fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
                let imgArr = PHAsset.fetchAssets(with: .image, options: fetchOptions) as PHFetchResult
                for i in 0 ..< imgArr.count {
                    self.imageAssets.append(imgArr[i])
                    print(self.imageAssets[i])
                }
                print(self.imageAssets.count)
                DispatchQueue.main.async {
                    self.collectionview.reloadData()
                }
            }

        }
    }
    func getAssetThumbnail(asset: PHAsset) -> UIImage? {
        
        let option = PHImageRequestOptions()
        option.isSynchronous = true
        var temp: UIImage?
        PHImageManager.default().requestImageData(for: asset, options: option) {  (imageData, dataUII, orientation, info) in
            if let data = imageData {
                temp = UIImage(data: data)!
            }
        }
        return temp
    }
    func setupSegmentedControl(){
        sgType.backgroundColor = .clear
        sgType.tintColor = .clear
        sgType.setTitleTextAttributes([
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14),
            NSAttributedString.Key.foregroundColor: UIColor.darkGray
            ], for: .normal)

        sgType.setTitleTextAttributes([
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14),
            NSAttributedString.Key.foregroundColor: UIColor.white
            ], for: .selected)
        buttonBar.translatesAutoresizingMaskIntoConstraints = false
        buttonBar.backgroundColor = UIColor.white
        view.addSubview(buttonBar)
        // Constrain the top of the button bar to the bottom of the segmented control
        buttonBar.topAnchor.constraint(equalTo: sgType.bottomAnchor).isActive = true
        buttonBar.heightAnchor.constraint(equalToConstant: 2).isActive = true
        buttonBar.leftAnchor.constraint(equalTo: sgType.leftAnchor, constant: view.frame.size.width / 16 * 3).isActive = true
        buttonBar.widthAnchor.constraint(equalTo: sgType.widthAnchor, multiplier: 1 / CGFloat(sgType.numberOfSegments*4)).isActive = true
    }

    @IBAction func segmentedControlDidChange(_ sender: Any) {
        UIView.animate(withDuration: 0.3) {
            let xValue = (self.sgType.frame.width / CGFloat(self.sgType.numberOfSegments)) * CGFloat(self.sgType.selectedSegmentIndex)
            self.buttonBar.frame.origin.x = xValue + (self.view.frame.size.width / CGFloat(16)) * CGFloat(3)
        }
        self.collectionview.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.sgType.selectedSegmentIndex == 0 {
            return imageAssets.count
        }else{
            return videoAssets.count
        }
        
    }
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectMediaCollectionViewCell", for: indexPath as IndexPath) as! SelectMediaCollectionViewCell
        if self.sgType.selectedSegmentIndex == 0 {
            cell.ivImage.image = self.getAssetThumbnail(asset: imageAssets[indexPath.row])
            
            if selectedImageArray.contains(String("\(indexPath.row)")) {
                cell.lblSelected.layer.cornerRadius = cell.lblSelected.frame.size.height/2
                cell.lblSelected.clipsToBounds = true
                cell.lblSelected.layer.borderWidth = 0
                cell.lblSelected.backgroundColor = UIColor.init(red: 255, green: 41, blue: 125)
            }else{
                cell.lblSelected.layer.cornerRadius = cell.lblSelected.frame.size.height/2
                cell.lblSelected.layer.borderWidth = 1
                cell.lblSelected.layer.borderColor = UIColor.white.cgColor
                cell.lblSelected.backgroundColor = UIColor.clear
            }
            return cell
        }else{
            cell.ivImage.image = self.getAssetThumbnail(asset: videoAssets[indexPath.row])
            
            if selectedVideoArray.contains(String("\(indexPath.row)")) {
                cell.lblSelected.layer.cornerRadius = cell.lblSelected.frame.size.height/2
                cell.lblSelected.clipsToBounds = true
                cell.lblSelected.layer.borderWidth = 0
                cell.lblSelected.backgroundColor = UIColor.init(red: 255, green: 41, blue: 125)
            }else{
                cell.lblSelected.layer.cornerRadius = cell.lblSelected.frame.size.height/2
                cell.lblSelected.layer.borderWidth = 1
                cell.lblSelected.layer.borderColor = UIColor.white.cgColor
                cell.lblSelected.backgroundColor = UIColor.clear
            }
            return cell
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        if self.sgType.selectedSegmentIndex == 0 {
            if (selectedImageArray.contains(String("\(indexPath.row)"))) {
                //selectedImageArray removeObjectAtIndex:[selectedImageArray indexOfObject:selectedRow]];
                selectedImageArray.remove(at: selectedImageArray.firstIndex(of:String("\(indexPath.row)"))!)
            }else{
                if selectedImageArray.count < nMaxCount {
                    selectedImageArray.append(String("\(indexPath.row)"))
                }else{
                    showErrorAlert(message: "Can not add more!")
                }
            }
            collectionview.reloadItems(at: [indexPath])
        }else{
            if (selectedVideoArray.contains(String("\(indexPath.row)"))) {
                //selectedImageArray removeObjectAtIndex:[selectedImageArray indexOfObject:selectedRow]];
                selectedVideoArray.remove(at: selectedVideoArray.firstIndex(of:String("\(indexPath.row)"))!)
            }else{
                if selectedVideoArray.count < nMaxCount {
                    selectedVideoArray.append(String("\(indexPath.row)"))
                }else{
                    showErrorAlert(message: "Can not add more!")
                }
            }
            collectionview.reloadItems(at: [indexPath])
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = (collectionView.frame.size.width - 30 ) / 4
        let cellHeight = cellWidth
        return CGSize(width: cellWidth, height: cellHeight)
    }
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController()
    }
    
}
