//
//  MediaViewController.swift
//  Fanbit
//
//  Created by fanbit on 2021/1/9.
//

import UIKit
import PhotosUI
class MediaViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var lblCount: UILabel!
    var imageAssets = [PHAsset]()
    override func viewDidLoad() {
        super.viewDidLoad()

        lblCount.text = String.init(format: "1/%d", imageAssets.count)
        let scrollview = UIScrollView.init(frame: viewContainer.frame)
        viewContainer.addSubview(scrollview)
        scrollview.isPagingEnabled = true
        scrollview.delegate = self
        for id in 0...imageAssets.count - 1 {
            let imageview = UIImageView.init(frame: CGRect(x: scrollview.frame.size.width * CGFloat(id), y: 0, width: scrollview.frame.size.width, height: scrollview.frame.size.height))
            imageview.image =  self.getAssetThumbnail(asset: imageAssets[id])
            imageview.contentMode = .scaleAspectFit
            scrollview.addSubview(imageview)
        }
        scrollview.contentSize = CGSize(width: scrollview.frame.size.width * CGFloat(imageAssets.count), height: scrollview.frame.size.height)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backClicked(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: UploadMediaViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    @IBAction func nextClicked(_ sender: Any) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "MainTabBarController") as! MainTabBarController
        self.navigationController?.pushViewController(newViewController, animated: false)
    }
    func getAssetThumbnail(asset: PHAsset) -> UIImage? {
        
        let option = PHImageRequestOptions()
        option.isSynchronous = true
        var temp: UIImage?
        PHImageManager.default().requestImageData(for: asset, options: option) {  (imageData, dataUII, orientation, info) in
            if let data = imageData {
                temp = UIImage(data: data)!
            }
        }
        return temp
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("END Scrolling \(scrollView.contentOffset.x / scrollView.bounds.size.width)")
        let index = Int(scrollView.contentOffset.x / scrollView.bounds.size.width)
        lblCount.text = String.init(format: "%d/%d", index + 1, imageAssets.count)
    }
}
