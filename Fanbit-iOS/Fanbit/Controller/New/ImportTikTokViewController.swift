//
//  ImportTikTokViewController.swift
//  Fanbit
//
//  Created by fanbit on 2021/1/9.
//

import UIKit
protocol SelectTikTokURLDelegate {
    func setTikTokURL(url:String)
    
}
class ImportTikTokViewController: UIViewController {
    var delegate : SelectTikTokURLDelegate?
    @IBOutlet weak var tfUrl: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func backClicked(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: UploadMediaViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    @IBAction func nextClicked(_ sender: Any) {
        if tfUrl.text == "" {
            showErrorAlert(message: "Please insert TikTok Url")
            return
        }
        self.delegate?.setTikTokURL(url: tfUrl.text!)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
