//
//  UploadMediaViewController.swift
//  Fanbit
//
//  Created by fanbit on 2021/1/8.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseStorage

class UploadMediaViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnImageVideo: UIButton!
    @IBOutlet weak var btnTiktok: UIButton!
    @IBOutlet weak var tvDesc: UITextView!
    @IBOutlet weak var tfProjectName: UITextField!
    var creatorSelected:User!
    var mediaURLs:[String] = []
    var isFrom = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        btnNext.layer.cornerRadius = btnNext.frame.size.height/2
        btnImageVideo.cornerRadius = btnImageVideo.frame.size.height/2
        btnTiktok.cornerRadius = btnTiktok.frame.size.height/2
        
        tvDesc.text = "Add description here..."
        tvDesc.textColor = UIColor.lightGray
        tvDesc.delegate = self
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
        
        if creatorSelected == nil {
            isFrom = "creator"
            guard let uid = Auth.auth().currentUser?.uid else
            { return }
            UserService.shared.fetchUser(uid: uid)
            { user in
                self.creatorSelected = user
            }
        }
        // Do any additional setup after loading the view.
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    @IBAction func backClicked(_ sender: Any) {

            if isFrom == "creator" { // in case of creator
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "MainTabBarController") as! MainTabBarController
                self.navigationController?.pushViewController(newViewController, animated: false)
            }else{
                self.navigationController?.popViewController()
            }

    }
   
    @IBAction func nextClicked(_ sender: Any) {
        if mediaURLs.count == 0 {
            showErrorAlert(message: "Medias were not selected!")
            return
        }
        if tvDesc.text == "" || tvDesc.text == "Add description here..." {
            showErrorAlert(message: "Add description!")
            return
        }
        if self.tfProjectName.text == "" {
            showErrorAlert(message: "Input project name!")
            return
        }
        // upload project
        ProjectService.shared.uploadProject(creatorSelected: creatorSelected.uid, mediaURLs:mediaURLs, description:tvDesc.text, projectName:self.tfProjectName.text!) { (error, ref) in
            if let error = error {
                print("DEBUG: Failed to upload project with error \(error.localizedDescription)")
                return
            }
            
            self.showSuccessAlert(message: "successfully uploaded!")
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "MainTabBarController") as! MainTabBarController
            self.navigationController?.pushViewController(newViewController, animated: false)
            
        }
    }
    @IBAction func imagevideoClicked(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "SelectMediaViewController") as! SelectMediaViewController
        newViewController.delegate = self
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    @IBAction func tiktokClicked(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "ImportTikTokViewController") as! ImportTikTokViewController
        newViewController.delegate = self
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if tvDesc.textColor == UIColor.lightGray {
            tvDesc.text = nil
            tvDesc.textColor = UIColor.white
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if tvDesc.text.isEmpty {
            tvDesc.text = "Add description here..."
            tvDesc.textColor = UIColor.lightGray
        }
    }
}
extension UploadMediaViewController: SelectMediaDelegate {
    func setUploadedMediaURLs(urls:[String]) {
        for url in urls {
            mediaURLs.append(url)
        }
    }
}
extension UploadMediaViewController: SelectTikTokURLDelegate {
    func setTikTokURL(url:String) {
           mediaURLs.append(url)
    }
}
