//
//  HomeTableViewCell.swift
//  Fanbit
//
//  Created by fanbit on 2020/12/29.
//

import UIKit
import Firebase
class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var ivProfile: UIImageView!
    @IBOutlet weak var ivBack: UIImageView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var ivCurrentUser: UIImageView!
    @IBOutlet weak var btnAddComment: UIButton!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblComments: UILabel!
    @IBOutlet weak var lblProjectName: UILabel!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var lblLikedBy: UILabel!
    @IBOutlet weak var lblCommentLeft: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        ivProfile.layer.cornerRadius = ivProfile.frame.width/2
        ivBack.layer.cornerRadius = 20
        lblTime.layer.cornerRadius = 5
        lblTime.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func initwithdata(project : Project){
        let creatorID = project.creatorSelected
        UserService.shared.fetchUser(uid: creatorID) { user in
            if user.profileImageUrl != nil{
                self.ivProfile.sd_setImage(with: user.profileImageUrl, completed: nil)
            }
            self.lblUsername.text = String.init(format: "Sponsored by %@", user.username)
        }
        self.lblProjectName.text = project.projectName
        let currentUserID = (Auth.auth().currentUser?.uid)!
        UserService.shared.fetchUser(uid: currentUserID) { user in
            if user.profileImageUrl != nil{
                self.ivCurrentUser.sd_setImage(with: user.profileImageUrl, completed: nil)
            }
        }
        self.ivBack.sd_setImage(with:URL(string: project.mediaURLs.first), completed: nil)
        lblTime.text = project.timestamp.dateString()
        ProjectService.shared.fetchLikeCountsForProject(projectID: project.projectID) { nCount in
            self.lblLike.text = String(nCount)
        }
        ProjectService.shared.fetchCommentsCountsForProject(projectID: project.projectID) { nCount in
            self.lblComments.text = String(nCount)
        }
        ProjectService.shared.checkLikedProject(projectID: project.projectID) { bLiked in
            if bLiked {
                self.btnLike.setImage(UIImage.init(named: "heart_fill"), for: .normal)
            }else{
                self.btnLike.setImage(UIImage.init(named: "heart"), for: .normal)
            }
        }
        var strLikedBy = ""
        ProjectService.shared.fetchLikedUsernamesByProject(projectID: project.projectID) { names in
            for name in names {
                strLikedBy = String.init(format: "%@ Liked By: %@",strLikedBy, name)
                
            }
            self.lblLikedBy.text = strLikedBy
        }
        ProjectService.shared.fetchCommentsByProject(projectID: project.projectID) { comments in
            if comments.count != 0{
                self.lblCommentLeft.text = comments.first?.comment
            }
        }
    }
}
