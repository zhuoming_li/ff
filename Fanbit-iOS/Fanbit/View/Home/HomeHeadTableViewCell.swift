//
//  HomeHeadTableViewCell.swift
//  Fanbit
//
//  Created by fanbit on 2021/1/15.
//

import UIKit
protocol CreatorSelectDelegate {
    func selectCreator(nIndex:Int)
    
}
class HomeHeadTableViewCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UICollectionViewDelegate  {

    @IBOutlet weak var collectionview: UICollectionView!// collection view for creators
    var userSelected:User!
    
    var delegate : CreatorSelectDelegate?
    private var creators = [User]()
    override func awakeFromNib() {
        super.awakeFromNib()
       
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func initwithdata(users:[User]){
        
        self.creators = users
        self.userSelected = users.first
       
        self.collectionview.delegate = self
        self.collectionview.dataSource = self
        self.collectionview.reloadData()
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return self.creators.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath as IndexPath) as! HomeCollectionViewCell
        cell.initwithdata(user:self.creators[indexPath.row])
       
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        self.delegate?.selectCreator(nIndex: indexPath.row)

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = collectionView.frame.size.height - 10
        let cellHeight = collectionView.frame.size.height
        return CGSize(width: cellWidth, height: cellHeight)
    }
}
