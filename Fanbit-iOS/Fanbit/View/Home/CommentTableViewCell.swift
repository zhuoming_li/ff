//
//  CommentTableViewCell.swift
//  Fanbit
//
//  Created by fanbit on 2021/1/15.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var ivCommenter: UIImageView!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblCommenterName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func initwithdata(comment:Comment){
        UserService.shared.fetchUser(uid: comment.commentedUser) { user in
            
            self.ivCommenter.sd_setImage(with: user.profileImageUrl, completed: nil)
            self.lblComment.text = comment.comment
            self.lblCommenterName.text = user.username
            self.lblTime.text = comment.timestamp.dateString()
        }
    }
}
