//
//  HomeCollectionViewCell.swift
//  Fanbit
//
//  Created by fanbit on 2020/12/29.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var ivBack: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var ivProfile: UIImageView!
    
    func initwithdata(user : User){
        if user.profileImageUrl != nil{
            self.ivProfile.sd_setImage(with: user.profileImageUrl, completed: nil)
        }
        self.lblUsername.text = user.username
        UserService.shared.checkCreatorHighLightStatus(creator: user.uid) { bHighLight in
            if bHighLight {
                self.ivBack.image = UIImage.init(named: "Ellipse_Sel")
            }else{
                self.ivBack.image = UIImage.init(named: "Ellipse")
            }
        }
    }
}
