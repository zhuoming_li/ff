//
//  ProjectCollectionViewCell.swift
//  Fanbit
//
//  Created by fanbit on 2021/1/10.
//

import UIKit

class ProjectCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var ivBack: UIImageView!
    func initwithdata(project:Project){
        self.ivBack.sd_setImage(with:URL(string: project.mediaURLs.first), completed: nil)
    }
}
