//
//  ActivityTableViewCell.swift
//  Fanbit
//
//  Created by fanbit on 2020/12/29.
//

import UIKit

class ActivityTableViewCell: UITableViewCell {
    @IBOutlet weak var lblState: UILabel!

    @IBOutlet weak var lblProjectName: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTime: UILabel!

    @IBOutlet weak var ivProfile: UIImageView!
    @IBOutlet weak var lblTime2: UILabel!
    @IBOutlet weak var ivBack: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        ivProfile.layer.cornerRadius = ivProfile.frame.size.height/2
        ivProfile.clipsToBounds = true
        lblState.layer.cornerRadius = 5
        lblState.clipsToBounds = true
        lblDescription.layer.cornerRadius = 5
        lblDescription.clipsToBounds = true
        lblTime2.layer.cornerRadius = 5
        lblTime2.clipsToBounds = true
        ivBack.layer.cornerRadius = 20
        ivBack.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func initwithdata(activity:Activity){
        ProjectService.shared.fetchProjectByID(projectID: activity.projectID!) { project in
            let creatorID = project.creatorSelected
            UserService.shared.fetchUser(uid: creatorID) { user in
                self.ivProfile.sd_setImage(with: user.profileImageUrl, completed: nil)
                self.lblUsername.text = String.init(format: "Sponsored by %@",  user.username)
            }
            self.lblProjectName.text = project.projectName
            self.ivBack.sd_setImage(with:URL(string: project.mediaURLs.first), completed: nil)
            self.lblTime2.text = project.timestamp.dateString()
            self.lblTime.text = project.timestamp.dateString()
            self.lblDescription.text = project.description
        }
        if activity.activity_type == ActivityType.startedProject {
            self.lblState.text = "You have started this project"
        } else if activity.activity_type == ActivityType.backedProject {
            self.lblState.text = "You have backed this project"
        }else if activity.activity_type == ActivityType.likedProject {
            self.lblState.text = "You liked this project"
        }else{
            self.lblState.text = "You have commented on this project"
        }
    }
}
