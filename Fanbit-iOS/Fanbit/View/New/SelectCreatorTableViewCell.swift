//
//  SelectCreatorTableViewCell.swift
//  Fanbit
//
//  Created by fanbit on 2021/1/8.
//

import UIKit
import SDWebImage

class SelectCreatorTableViewCell: UITableViewCell {

    @IBOutlet weak var ivProfile: UIImageView!
    @IBOutlet weak var ivType: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var ivBack: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func initwithdata(user : User){
        self.ivProfile.sd_setImage(with: user.profileImageUrl, completed: nil)
        self.lblName.text = user.username
    }
}
