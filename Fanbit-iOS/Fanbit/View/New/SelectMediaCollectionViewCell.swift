//
//  SelectMediaCollectionViewCell.swift
//  Fanbit
//
//  Created by fanbit on 2021/1/9.
//

import UIKit

class SelectMediaCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var ivImage: UIImageView!
    @IBOutlet weak var lblSelected: UILabel!
    
}
