//
//  InboxTableViewCell.swift
//  Fanbit
//
//  Created by fanbit on 2021/1/8.
//

import UIKit

class InboxTableViewCell: UITableViewCell {

    @IBOutlet weak var ivProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnConfirm: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func initwithdata(notification:Notification){
        if notification.type == NotificationType.submitted {
            ProjectService.shared.fetchProjectByID(projectID: notification.projectID!) { project in
                UserService.shared.fetchUser(uid: project.maker) { user in
                    
                    self.ivProfile.sd_setImage(with: user.profileImageUrl, completed: nil)
                    self.lblName.text = user.username
                    self.lblTime.text = notification.timestamp.dateString()
                    
                    if notification.status == NotificationStatus.accepted{
                        
                        self.lblMessage.text = "You accepted the project"
                        self.btnConfirm.isHidden = true
                    }else{
                        self.lblMessage.text = "Suggested the project for you"
                        self.btnConfirm.isHidden = false
                    }
                }
            }
        }else if notification.type == NotificationType.liked{
            UserService.shared.fetchUser(uid: notification.fromUser!) { user in
                
                self.ivProfile.sd_setImage(with: user.profileImageUrl, completed: nil)
                self.lblName.text = user.username
                self.lblTime.text = notification.timestamp.dateString()
                if user.usertype == CREATOR {
                    self.lblMessage.text = "liked the project what you accepted"
                }else{
                    self.lblMessage.text = "liked the project what you started"
                }
            }
        }else{
            UserService.shared.fetchUser(uid: notification.fromUser!) { user in
                
                self.ivProfile.sd_setImage(with: user.profileImageUrl, completed: nil)
                self.lblName.text = user.username
                self.lblTime.text = notification.timestamp.dateString()
                if user.usertype == CREATOR {
                    self.lblMessage.text = "made comment on the project what you accepted"
                }else{
                    self.lblMessage.text = "made comment the project what you started"
                }
            }
        }
    }
}
