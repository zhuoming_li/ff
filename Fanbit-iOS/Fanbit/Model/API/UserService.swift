//
//  UserService.swift
//  Fanbit
//
//  Created by fanbit on 2020/12/29.


import Firebase
import FirebaseAuth
import FirebaseDatabase

typealias DatabaseCompletion = ((Error?, DatabaseReference) -> Void)


struct UserService {
    static let shared = UserService()
    
    func fetchUser(uid: String, completion: @escaping(User) -> Void) {
        REF_USERS.child(uid).observeSingleEvent(of: .value) { snapshot in
            guard let dictionary = snapshot.value as? [String: AnyObject] else { return }
            let user = User(uid: uid, dictionary: dictionary)
            completion(user)
        }
    }
    func fetchCreators(completion: @escaping([User]) -> Void) {
        var users = [User]()
        REF_USERS.queryOrdered(byChild: "usertype").queryEqual(toValue: CREATOR).observeSingleEvent(of: .value) { snapshot in
            guard let dictionary = snapshot.value as? NSDictionary else { return }
            for key in dictionary.allKeys{
                let user = User(uid: key as! String, dictionary: (dictionary.value(forKey: key as! String) as? [String: AnyObject])!)
                users.append(user)
            }
            completion(users)
         }
    }
    func updateProfileImage(image: UIImage, completion: @escaping(URL?) -> Void) {
        guard let imageData = image.jpegData(compressionQuality: 0.3) else { return }
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let filename = NSUUID().uuidString
        let ref = STORAGE_PROFILE_IMAGES.child(filename)
        
        ref.putData(imageData, metadata: nil) { (meta, error) in
            ref.downloadURL { (url, error) in
                guard let profileImageUrl = url?.absoluteString else { return }
                let values = ["profileImageUrl": profileImageUrl]
                
                REF_USERS.child(uid).updateChildValues(values) { (err, ref) in
                    completion(url)
                }
            }
        }
    }
    func checkEmailWasAlreadyTaken(email:String,completion: @escaping(Bool) -> Void) {
        REF_USERS.queryOrdered(byChild: "email").queryEqual(toValue: email).observeSingleEvent(of: .value) { snapshot in
                completion(snapshot.exists())
        }
    }
    func saveUserData(user: User, completion: @escaping(DatabaseCompletion)) {
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let values = ["username": user.username, "bio": user.bio, "profileImageUrl":user.profileImageUrl?.absoluteString] as [String : Any]
        REF_USERS.child(uid).updateChildValues(values, withCompletionBlock: completion)
    }
    func followUser(uid: String, completion: @escaping(DatabaseCompletion)) {
        guard let currentUid = Auth.auth().currentUser?.uid else { return }
        
        REF_USER_FOLLOWING.child(currentUid).updateChildValues([uid: 1]) { (err, ref) in
            REF_USER_FOLLOWERS.child(uid).updateChildValues([currentUid: 1], withCompletionBlock: completion)
        }
    }
    func checkIfUserIsFollowed(uid: String, completion: @escaping(Bool) -> Void) {
        guard let currentUid = Auth.auth().currentUser?.uid else { return }

        REF_USER_FOLLOWING.child(currentUid).child(uid).observeSingleEvent(of: .value) { snapshot in
            completion(snapshot.exists())
        }
    }
    func checkUserType(completion: @escaping(String) -> Void) {
        guard let uid = Auth.auth().currentUser?.uid else { return }
                REF_USERS.child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
                    if let value = snapshot.value as? NSDictionary{
                        if let usertype = value["usertype"] as? String{
                            if usertype == CREATOR { // if user is creator
                               completion(CREATOR)
                            }else{
                                completion(NORMAL_USER)
                            }
                        }
                    }
                  }) { (error) in
                    print(error.localizedDescription)
                }
    }
    func fetchFollowingUsers(uid:String, completion:@escaping([User]) -> Void) {
        var users = [User]()
        REF_USER_FOLLOWING.child(uid).observeSingleEvent(of: .value) { snapshot in
            guard let dictionary = snapshot.value as? NSDictionary else { return }
            for key in dictionary.allKeys{
                REF_USERS.child(key as! String).observeSingleEvent(of: .value) { snapshot in
                    guard let dic = snapshot.value as? [String: AnyObject] else { return }
                    let user = User(uid: uid, dictionary: dic)
                    users.append(user)
                    if users.count == dictionary.count{
                        completion(users)
                    }
                }
            }
         }
    }
    func checkCreatorHighLightStatus(creator:String, completion: @escaping(Bool) -> Void) {
        REF_PROJECTS.observeSingleEvent(of: .value) { snapshot in
            guard let dictionary = snapshot.value as? NSDictionary else { completion(false); return }
            for key in dictionary.allKeys{
                let dic = dictionary[key] as! [String : Any]
                if (dic["maker"] as! String == creator) || (dic["creatorSelected"] as! String == creator && dic["status"] as! Int == PROJECT_STATUS.Active.rawValue) {
                    completion(true)
                    return
                }
            }
            completion(false)
        }
    }
    
}
