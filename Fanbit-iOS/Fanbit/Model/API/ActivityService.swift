//
//  ActivityService.swift
//  Fanbit
//
//  Created by fanbit on 2021/1/16.
//

import Foundation
import Firebase

struct ActivityService {
    static let shared = ActivityService()
    func uploadActivity(userID: String,
                            type: ActivityType,
                            projectID: String) {
        let values: [String: Any] = ["timestamp": Int(NSDate().timeIntervalSince1970),
                                     "projectID": projectID,
                                     "type": type.rawValue as Any]
        REF_ACTIVITY.child(userID).childByAutoId().updateChildValues(values)
    }
    func fetchActivities(uid: String, completion: @escaping([Activity]) -> Void) {
        var activities = [Activity]()

        REF_ACTIVITY.child(uid).observeSingleEvent(of: .value) { snapshot in
            guard let dictionary = snapshot.value as? NSDictionary else { return }
            for key in dictionary.allKeys{
                let dic = dictionary[key] as? NSDictionary
                let activity = Activity.init(dictionary: dic as! [String : AnyObject])
                activities.append(activity)
            }
            completion(activities)
         }
    }
}
