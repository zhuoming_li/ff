//
//  ProjectManager.swift
//  Fanbit
//
//  Created by fanbit on 2021/1/13.
//

import Foundation
import Firebase
import FirebaseAuth
import FirebaseDatabase

struct ProjectService {
    static let shared = ProjectService()
    
    func uploadProject(creatorSelected: String, mediaURLs: [String], description:String, projectName:String, completion: @escaping(DatabaseCompletion)) {
        guard let currentId = Auth.auth().currentUser?.uid else { return }
        let values = ["creatorSelected": creatorSelected,
                      "timestamp": Int(NSDate().timeIntervalSince1970),
                      "mediaURLs": mediaURLs,
                      "maker": currentId,
                      "description" : description,"projectName":projectName,
                      "status": currentId == creatorSelected ? PROJECT_STATUS.Active.rawValue : PROJECT_STATUS.Pending.rawValue] as [String : Any]
         REF_PROJECTS.childByAutoId().updateChildValues(values) { (err, postRef) in
                // update user-post structure after post upload completes
                guard let projectID = postRef.key else { return }
            NotificationService.shared.uploadNotification(toUser: creatorSelected, type: .submitted, projectID: projectID, status: .pending)
            ActivityService.shared.uploadActivity(userID: currentId, type: ActivityType.startedProject, projectID: projectID)
            REF_USER_POST_PROJECTS.child(currentId).updateChildValues([projectID: 1]) { (err, userPostRef) in
                completion(err, postRef)
            }
         }
     }
    func updateProject(projectID:String, values:[String:Any],completion: @escaping(DatabaseCompletion)){
        
        REF_PROJECTS.child(projectID).updateChildValues(values){
            (err, postRef) in
            completion(err, postRef)
        }
        guard let currentId = Auth.auth().currentUser?.uid else { return }
        REF_USER_POST_PROJECTS.child(currentId).updateChildValues([projectID: 1]) { (err, userPostRef) in
            completion(err, userPostRef)
        }
    }
    func fetchProjectsStartedByUser(uid:String, completion: @escaping([Project]) -> Void) {
        var projects = [Project]()
        REF_USER_POST_PROJECTS.child(uid).observeSingleEvent(of: .value) { snapshot in
            guard let dictionary = snapshot.value as? NSDictionary else { return }
            for key in dictionary.allKeys{
                REF_PROJECTS.child(key as! String).observeSingleEvent(of: .value) { snapshot in
                    guard let dict = snapshot.value as? NSDictionary else { return }
                    let project = Project.init(projectID: key as! String, dictionary: dict as! [String : Any])
                    projects.append(project)
                    if projects.count == dictionary.allKeys.count{
                        completion(projects)
                    }
                }
            }
         }
    }
    func fetchProjectsSubmittedToCreator(uid:String, completion: @escaping([Project]) -> Void) {
        var projects = [Project]()
        REF_PROJECTS.queryOrdered(byChild: "creatorSelected").queryEqual(toValue: uid).observeSingleEvent(of: .value) { snapshot in
            guard let dictionary = snapshot.value as? NSDictionary else { return }
            for key in dictionary.allKeys{
                let project = Project.init(projectID: key as! String, dictionary: (dictionary.value(forKey: key as! String) as? [String: AnyObject])!)
                projects.append(project)
                if projects.count == dictionary.allKeys.count{
                    completion(projects)
                }
            }
         }
       
    }
    func fetchActiveProjects(completion: @escaping([Project]) -> Void) {
        var projects = [Project]()
        REF_PROJECTS.queryOrdered(byChild: "status").queryEqual(toValue: PROJECT_STATUS.Active.rawValue).observeSingleEvent(of: .value) { snapshot in
            guard let dictionary = snapshot.value as? NSDictionary else { return }
            for key in dictionary.allKeys{
                let project = Project.init(projectID: key as! String, dictionary: (dictionary.value(forKey: key as! String) as? [String: AnyObject])!)
                projects.append(project)
            }
            completion(projects)
         }
    }
    func fetchProjectByID(projectID:String, completion:@escaping(Project) -> Void) {
        REF_PROJECTS.child(projectID).observeSingleEvent(of: .value) { snapshot in
            guard let dictionary = snapshot.value as? [String:Any] else { return }
            let project = Project.init(projectID: projectID, dictionary: dictionary)
            completion(project)
        }
        
    }
    func likeProject(projectID:String, completion: @escaping(DatabaseCompletion)) {
        
        guard let currentId = Auth.auth().currentUser?.uid else { return }
        let values = ["likedUser": currentId,
                      "timestamp": Int(NSDate().timeIntervalSince1970)] as [String : Any]
        REF_PROJECT_LIKES.child(projectID).childByAutoId().updateChildValues(values) { (err, postRef) in
                // add this to notification
            ProjectService.shared.fetchProjectByID(projectID: projectID) { project in
                NotificationService.shared.uploadNotification(toUser: project.creatorSelected, type: .liked, projectID: projectID, status: .accepted)
                NotificationService.shared.uploadNotification(toUser: project.maker, type: .liked, projectID: projectID, status: .accepted)
               
                completion(err, postRef)
            }
            REF_USER_POST_PROJECTS.child(currentId).updateChildValues([projectID: 1]) { (err, userPostRef) in
                completion(err, postRef)
            }
         }
    }
    func backProject(projectID:String,price:Float, completion: @escaping(DatabaseCompletion)) {
        guard let currentId = Auth.auth().currentUser?.uid else { return }
        REF_PROJECT_BACKED.child(projectID).updateChildValues([currentId: price]) { (err, ref) in
            REF_USER_POST_PROJECTS.child(currentId).updateChildValues([projectID: 1])
            ProjectService.shared.fetchProjectByID(projectID: projectID) { project in
                let fundsRaised = project.fundsRaised + price
                let numberBackers = project.numberOfBackers + 1
                ProjectService.shared.updateProject(projectID: projectID, values: ["fundsRaised":fundsRaised,"numberOfBackers":numberBackers]) { (err, ref) in
                    
                }
            }
        }
    }
    func checkBacked(projectID: String, completion: @escaping(Bool) -> Void) {
        guard let currentUid = Auth.auth().currentUser?.uid else { return }
        REF_PROJECT_BACKED.child(projectID).child(currentUid).observeSingleEvent(of: .value) { snapshot in
            completion(snapshot.exists())
        }
    }
    func checkLikedProject(projectID: String, completion: @escaping(Bool) -> Void) {
        guard let currentUid = Auth.auth().currentUser?.uid else { return }
        
        REF_PROJECT_LIKES.child(projectID).queryOrdered(byChild: "likedUser").queryEqual(toValue: currentUid).observeSingleEvent(of: .value) { snapshot in
            completion(snapshot.exists())
        }
    }
    func unlikeProject(projectID:String, completion: @escaping(DatabaseCompletion)) {
        guard let currentUid = Auth.auth().currentUser?.uid else { return }
        REF_PROJECT_LIKES.child(projectID).queryOrdered(byChild: "likedUser").queryEqual(toValue: currentUid).observeSingleEvent(of: .value) { snapshot in
            guard let dictionary = snapshot.value as? NSDictionary else { return }
            
            print(dictionary.allKeys.first)
            REF_PROJECT_LIKES.child(projectID).child(dictionary.allKeys.first as! String).removeValue(completionBlock: completion)
        }
    }
    func fetchLikeCountsForProject(projectID:String, completion:@escaping(Int)-> Void){
        REF_PROJECT_LIKES.child(projectID).observeSingleEvent(of: .value) { snapshot in
            guard let dictionary = snapshot.value as? NSDictionary else {
                completion(0)
                return }
            completion(dictionary.allKeys.count)
        }
    }
    func fetchCommentsCountsForProject(projectID:String, completion:@escaping(Int)-> Void){
        REF_PROJECT_COMMENTS.child(projectID).observeSingleEvent(of: .value) { snapshot in
            guard let dictionary = snapshot.value as? NSDictionary else {completion(0)
                return }
            completion(dictionary.allKeys.count)
        }
    }
    func commentProject(projectID:String, comment:String, completion: @escaping(DatabaseCompletion)) {
        
        guard let currentId = Auth.auth().currentUser?.uid else { return }
        let values = ["commentedUser": currentId, "comment":comment,
                      "timestamp": Int(NSDate().timeIntervalSince1970)] as [String : Any]
        REF_PROJECT_COMMENTS.child(projectID).childByAutoId().updateChildValues(values) { (err, postRef) in
                // add this to notification
            ProjectService.shared.fetchProjectByID(projectID: projectID) { project in
                NotificationService.shared.uploadNotification(toUser: project.creatorSelected, type: .commented, projectID: projectID, status: .accepted)
                NotificationService.shared.uploadNotification(toUser: project.maker, type: .commented, projectID: projectID, status: .accepted)
               
                completion(err, postRef)
            }
         }
    }
    func checkCommentedProject(projectID: String, completion: @escaping(Bool) -> Void) {
        guard let currentUid = Auth.auth().currentUser?.uid else { return }
        
        REF_PROJECT_COMMENTS.child(projectID).queryOrdered(byChild: "commentedUser").queryEqual(toValue: currentUid).observeSingleEvent(of: .value) { snapshot in
            completion(snapshot.exists())
        }
    }
    func fetchCommentsByProject(projectID:String, completion:@escaping([Comment]) -> Void) {
        var comments = [Comment]()
        REF_PROJECT_COMMENTS.child(projectID).observeSingleEvent(of: .value) { snapshot in
            guard let dictionary = snapshot.value as? NSDictionary else { return }
            for key in dictionary.allKeys{
                let comment = Comment.init(projectID: key as! String, dictionary: (dictionary.value(forKey: key as! String) as? [String: AnyObject])!)
                comments.append(comment)
            }
            completion(comments)
         }
    }
    func fetchLikedUsernamesByProject(projectID:String, completion:@escaping([String]) -> Void){
        var names = [String]()
        REF_PROJECT_LIKES.child(projectID).observeSingleEvent(of: .value) { snapshot in
            guard let dictionary = snapshot.value as? NSDictionary else { return }
            for key in dictionary.allKeys{
                let dic = dictionary[key] as! [String: AnyObject]
                UserService.shared.fetchUser(uid: dic["likedUser"] as! String) { user in
                    names.append(user.username)
                    if names.count == dictionary.allKeys.count{
                        completion(names)
                    }
                }
            }
        }
    }
}
