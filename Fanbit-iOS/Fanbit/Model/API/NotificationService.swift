//
//  NotificationService.swift
//  Fanbit
//
//  Created by fanbit on 2021/1/15.
//

import Foundation
import Firebase

struct NotificationService {
    static let shared = NotificationService()
    
    func uploadNotification(toUser: String,
                            type: NotificationType,
                            projectID: String, status: NotificationStatus) {
        let fromUser = Auth.auth().currentUser?.uid
        let values: [String: Any] = ["timestamp": Int(NSDate().timeIntervalSince1970),
                                     "projectID": projectID,
                                     "type": type.rawValue, "status":status.rawValue, "fromUser":fromUser as Any]
        REF_NOTIFICATIONS.child(toUser).childByAutoId().updateChildValues(values)
    }
    
    func fetchNotifications(uid: String, completion: @escaping([Notification]) -> Void) {
        var notifications = [Notification]()

        REF_NOTIFICATIONS.child(uid).observeSingleEvent(of: .value) { snapshot in
            guard let dictionary = snapshot.value as? NSDictionary else { return }
            for key in dictionary.allKeys{
                let dic = dictionary[key] as? NSDictionary
                let notification = Notification.init(toUser: uid, dictionary: dic as! [String : AnyObject])
                notifications.append(notification)
            }
            completion(notifications)
         }
    }
    
    func updateNotification(uid:String,projectID:String, values:[String:Any],completion: @escaping(DatabaseCompletion)){
        REF_NOTIFICATIONS.child(uid).queryOrdered(byChild: "projectID").queryEqual(toValue: projectID).observeSingleEvent(of: .value) { snapshot in
            let dic = snapshot.value as? NSDictionary
           let key = dic?.allKeys.first as? String
            REF_NOTIFICATIONS.child(uid).child(key!).updateChildValues(values){(err, postRef) in
                completion(err, postRef)
            }
        }
    }
}
