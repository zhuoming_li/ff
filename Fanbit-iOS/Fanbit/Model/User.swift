//
//  User.swift
//  Fanbit
//
//  Created by fanbit on 2020/12/29.
//

import Foundation
import Firebase
import FirebaseAuth

struct User {
    let email: String
    var username: String
   
    var profileImageUrl: URL?
    let uid: String
    var usertype: String?
    var bio:String
    var isCurrentUser: Bool { return Auth.auth().currentUser?.uid == uid }
    
   
    init(uid: String, dictionary: [String: AnyObject]) {
        self.uid = uid
        self.email = dictionary["email"] as? String ?? ""
        self.username = dictionary["username"] as? String ?? ""
        self.usertype = dictionary["usertype"] as? String ?? ""
        self.bio = dictionary["bio"] as? String ?? ""
        
        if let profileImageUrlString = dictionary["profileImageUrl"] as? String {
            guard let url = URL(string: profileImageUrlString) else { return }
            self.profileImageUrl = url
        }
    }
    
    init(dictionary: [String: Any]) {
        self.uid = dictionary["uid"] as? String ?? ""
        self.email = dictionary["email"] as? String ?? ""
        self.username = dictionary["username"] as? String ?? ""
        self.usertype = dictionary["usertype"] as? String ?? ""
        self.bio = dictionary["bio"] as? String ?? ""
        
        if let profileImageUrlString = dictionary["profileImageUrl"] as? String {
            guard let url = URL(string: profileImageUrlString) else { return }
            self.profileImageUrl = url
        }
    }
}

