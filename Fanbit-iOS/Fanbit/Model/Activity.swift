//
//  Activity.swift
//  Fanbit
//
//  Created by fanbit on 2021/1/16.
//

import Foundation
enum ActivityType: Int {
    case startedProject
    case backedProject
    case acceptedProject
    case likedProject
    case commentedProject
}
struct Activity {
    var activity_type:ActivityType!
    var projectID:String?
    var timestamp: Date!
    init(dictionary: [String: AnyObject]) {
        
        if let projectID = dictionary["projectID"] as? String {
            self.projectID = projectID
        }
        
        if let timestamp = dictionary["timestamp"] as? Double {
            self.timestamp = Date(timeIntervalSince1970: timestamp)
        }
        
        if let type = dictionary["type"] as? Int {
            self.activity_type = ActivityType(rawValue: type)
        }
    }
}
