//
//  Data.swift
//  Fanbit
//
//  Created by fanbit on 2020/12/29.
//

import Foundation
import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

let Pink_Color = UIColor.init(red: 255.0/255.0, green: 41.0/255.0, blue: 125.0/255.0, alpha: 1.0)
let Gray_Color = UIColor.darkGray

let CREATOR = "creator"
let NORMAL_USER = "normaluser"

let STORAGE_REF = Storage.storage().reference()
let STORAGE_PROFILE_IMAGES = STORAGE_REF.child("profile_images")
let STORAGE_PROJECT_IMAGES = STORAGE_REF.child("project_images")


let DB_REF = Database.database().reference()
let REF_USERS = DB_REF.child("users")
let REF_PROJECTS = DB_REF.child("projects")
let REF_USER_POST_PROJECTS = DB_REF.child("user_post_projects")
let REF_USER_FOLLOWING = DB_REF.child("user_following")
let REF_USER_FOLLOWERS = DB_REF.child("user_followers")
let REF_NOTIFICATIONS = DB_REF.child("notifications")
let REF_PROJECT_LIKES = DB_REF.child("project_likes")
let REF_PROJECT_BACKED = DB_REF.child("project_backed")
let REF_PROJECT_COMMENTS = DB_REF.child("project_comments")

let REF_ACTIVITY = DB_REF.child("activity")
