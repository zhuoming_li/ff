//
//  Notification.swift
//  Fanbit
//
//  Created by fanbit on 2021/1/15.
//

import Foundation
enum NotificationType: Int {
    case submitted
    case liked
    case commented
}
enum NotificationStatus: Int {
    case pending
    case accepted
}
struct Notification {
    var projectID: String?
    var timestamp: Date!
    var toUser: String
    var fromUser: String?
    var type: NotificationType!
    var status: NotificationStatus!
    init(toUser: String, dictionary: [String: AnyObject]) {
        self.toUser = toUser
        
        if let projectID = dictionary["projectID"] as? String {
            self.projectID = projectID
        }
        
        if let timestamp = dictionary["timestamp"] as? Double {
            self.timestamp = Date(timeIntervalSince1970: timestamp)
        }
        
        if let type = dictionary["type"] as? Int {
            self.type = NotificationType(rawValue: type)
        }
        if let status = dictionary["status"] as? Int {
            self.status = NotificationStatus(rawValue: status)
        }
        if let fromUser = dictionary["fromUser"] as? String {
            self.fromUser = fromUser
        }
    }
}
