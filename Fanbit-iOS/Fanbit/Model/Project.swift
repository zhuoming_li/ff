//
//  Project.swift
//  Fanbit
//
//  Created by fanbit on 2021/1/13.
//

import Foundation
enum PROJECT_STATUS : Int {
    case Pending
    case Active
    case InProgress
    case Delivered
    case Cancelled
}

struct Project {
    let projectID:String
    let timestamp:Date!
    let creatorSelected:String // id of the creator selected
    let maker:String // id of the user who made the project
    let status:PROJECT_STATUS.RawValue
    let mediaURLs:[String]
    let description:String
    let projectName:String
    let fundsRaised:Float
    let numberOfBackers:Int
    init(projectID: String, dictionary: [String: Any]) {
        self.projectID = projectID
        
        if let timestamp = dictionary["timestamp"] as? Double {
            self.timestamp = Date(timeIntervalSince1970: timestamp)
        }else{
            self.timestamp = nil
        }
        self.creatorSelected = dictionary["creatorSelected"] as? String ?? ""
        self.maker = dictionary["maker"] as? String ?? ""
        self.status = dictionary["status"] as? Int ?? 0
        self.mediaURLs = dictionary["mediaURLs"] as? [String] ?? []
        self.description = dictionary["description"] as? String ?? ""
        self.projectName = dictionary["projectName"] as? String ?? ""
        self.fundsRaised =  dictionary["fundsRaised"] as? Float ?? 0.0
        self.numberOfBackers = dictionary["numberOfBackers"] as? Int ?? 0
    }
}
struct Like{
    let projectID:String
    let likedUser:String
    let timestamp:Date!
    init(projectID:String, dictionary:[String: Any]){
        self.projectID = projectID
        if let timestamp = dictionary["timestamp"] as? Double {
            self.timestamp = Date(timeIntervalSince1970: timestamp)
        }else{
            self.timestamp = nil
        }
        self.likedUser = dictionary["likedUser"] as? String ?? ""
    }
}
struct Comment{
    let projectID:String
    let commentedUser:String
    let timestamp:Date!
    let comment:String
    init(projectID:String, dictionary:[String: Any]){
        self.projectID = projectID
        if let timestamp = dictionary["timestamp"] as? Double {
            self.timestamp = Date(timeIntervalSince1970: timestamp)
        }else{
            self.timestamp = nil
        }
        self.commentedUser = dictionary["commentedUser"] as? String ?? ""
        self.comment = dictionary["comment"] as? String ?? ""
    }
}

