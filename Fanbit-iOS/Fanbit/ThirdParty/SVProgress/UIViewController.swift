//
//  UIViewController.swift
//  FireAuth
//
//  Created by allzone on 10/3/18.
//  Copyright © 2018 iBIT Group. All rights reserved.
//

import UIKit
import SVProgressHUD

extension UIViewController {
    
    // MARK: Dismiss keyboard method
    @IBAction func dismissKeyboard() {
        view.endEditing(true)
    }
    
    // MARK: HUD indicator utils
    func showHUD() {
        SVProgressHUD.show()
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.setHapticsEnabled(true)
    }
    
    func hideHUD() {
        SVProgressHUD.dismiss()
    }
    
    // MARK: Alert utils
    func showErrorAlert(message: String?) {
        SVProgressHUD.showError(withStatus: message ?? "")
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.setDefaultMaskType(.gradient)
        SVProgressHUD.setMinimumDismissTimeInterval(1)
    }
    func showSuccessAlert(message: String?) {
        SVProgressHUD.showSuccess(withStatus: message ?? "")
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.setDefaultMaskType(.gradient)
        SVProgressHUD.setMinimumDismissTimeInterval(1)
    }
    func showAlertErrorIfNeeded(error: Error?) {
        if let e = error {
            showErrorAlert(message: e.localizedDescription)
        } else {
            hideHUD()
        }
    }
}
