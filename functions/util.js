// Following gives the default structure of an object.
const functions = require('firebase-functions');

class Util {
    getDefaultUser() {
        return {
            "username": "",
            "email": "",
            "fundEntries": [],
            "followingProjects": [],
            "followingCreators": [],
            "createdProjects": [],
            "backedProjects": [],
        }
    }

    getDefaultCreator() {
        return {
            "username": "",
            "email": "",
            "activeProjects": [],
            "pendingProjects": [],
            "finishedProjects": [],
        }
    }

    getDefaultProject() {
        return {
            "name": "",
            "creator": "",
            "pitcher": "",
            "backers": [],
            "fundTarget": 100,
            "fundRaised": 0,
            "fundEntries": [],
            "deadline": "",
            "status": "",
        }
    }

    getDefaultFundEntry() {
        return {
            "project": "",
            "user": "",
            "amount": 0,
            "timestamp": "",
            "status": "",
        }
    }
}

var util = new Util();

module.exports = util;