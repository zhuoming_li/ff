// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access Cloud Firestore.
const admin = require('firebase-admin');

const util = require('./util.js');

admin.initializeApp();


async function addProjectToMaker(db, proj, maker) {
  const mRef = db.collection('makers').doc(maker);
  const doc = await mRef.get();
	if (!doc.exists) {
	  console.log('No such maker!');
	} else {
	  console.log('Maker data:', doc.data());
  }
  console.log(doc.data())
  var projs = doc.data()['projects'];
  if (projs) {
    projs.push(proj);
  } else {
    projs = [proj];
  }
  return await mRef.set({projects : projs}, {merge: true});
}

// ================ Functions ==================
exports.addProject = functions.https.onRequest(async (req, res) => {
  var p = util.getDefaultProject();
  // The maker of the video
  const maker = req.query.maker;
  // The pitcher of this request / who started this request
  const pitcher = req.query.pitcher;
  p['maker'] = maker;
  p['pitcher'] = pitcher;
  // Push the new message into Cloud Firestore using the Firebase Admin SDK.
  const db = admin.firestore();
  const writeResult = await db.collection('projects').add(p);

  addProjectToMaker(db, writeResult.id, maker);

  // Send back a message that we've successfully written the message
  res.json({result: `Project with ID: ${writeResult.id} added.`});
});

exports.addMaker = functions.https.onRequest(async (req, res) => {
  // The maker of the video
  const maker = req.query.maker;
  const email = req.query.email;
  const writeResult = await admin.firestore().collection('makers').doc(maker).set({maker: maker, email : email});
  // Send back a message that we've successfully added the maker
  res.json({result: `Maker: ${maker} added.`});
});

exports.addUser = functions.https.onRequest(async (req, res) => {
  // Fans
  const user = req.query.user;
  const email = req.query.email;
  const writeResult = await admin.firestore().collection('users').doc(user).set({user: user, email : email});
  res.json({result: `User: ${user} added.`});
});

exports.queryMaker = functions.https.onRequest(async (req, res) => {
  // The maker of the video
  const maker = req.query.maker;
  const mRef = admin.firestore().collection('makers').doc(maker);
  const doc = await mRef.get();
	if (!doc.exists) {
	  console.log('No such maker!');
	} else {
	  console.log('Maker data:', doc.data());
  }
  return doc.data();
});

exports.addFund = functions.https.onRequest(async (req, res) => {
  // Fans
  const user = req.query.user;
  const amount = parseInt(req.query.amount);
  const proj = req.query.proj;
  const result = await admin.firestore().collection('projects').doc(proj).update({
    fundRaised: admin.firestore.FieldValue.increment(amount)
  });
  res.json({result: `fundRaised: ${amount} added from ${user} for ${proj}.`});
});

// ========== Listeners ==============
// Listens for new messages added to /messages/:documentId/original and creates an
// uppercase version of the message to /messages/:documentId/uppercase
exports.createDefaultFundSize = functions.firestore.document('/projects/{documentId}')
    .onCreate((snap, context) => {
      // Grab the current value of what was written to Cloud Firestore.
      const maker = snap.data().maker;

      // Access the parameter `{documentId}` with `context.params`
      functions.logger.log('Creating default size', context.params.documentId, maker);

      // Now we can check the Youtube API to see if the maker has a big audience and give coresponding fund size.
      
      const fundSize = maker == "justin_bieber" ? 1000000 : 100;
      
      // You must return a Promise when performing asynchronous tasks inside a Functions such as
      // writing to Cloud Firestore.
      // Setting an 'uppercase' field in Cloud Firestore document returns a Promise.
      return snap.ref.set({fundSize : fundSize}, {merge: true});
    });